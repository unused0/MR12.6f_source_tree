&command_line off
&
&label daemon_start_up
&
set_search_rules >system_library_tools>Daemon.search_rules
&if [equal [substr [user device_channel] 1 3] tty] &then &quit
iocall attach error_i/o mrd_ [user device_channel]
iocall attach error_output syn error_i/o
&
&quit
