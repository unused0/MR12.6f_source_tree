" make_rgr_commands.qedx:  qedx macro which examines all known devices
" at a site and creates an ec to register them under RCP Resource Management.

" Modified 1985-03-21 by E. Swenson to anticipate query from sort_seg
"   and to quit out of qedx with the force option.

e delete register.ec rcp.list -bf

" Produce a list of the configured devices.

e file_output rcp.list
e rcp_list -lg
e revert_output

" Read in output from rcp_list command

r rcp.list
1,3d                                    " kill header

" create subroutine to move line to building buffer

b(add_build) a
m(temp)                                 " first move the line to temp
b(build)                                " then go into build and append
a
\c\c\c\b(temp)\c\c\c\f                  " the line in temp
b(0)                                    " return to main buffer
\f                                      " end subroutine

" create subroutine to clear the building buffer

b(clear_build) a
b(build) a
xxx
\c\f
1,$d
b(0)
\f

b(0)                                    " return to main buffer

" process tape drives

/Device type: "tape_drive"/n            " find right section

" set up buffer program to create registration command from each tape entry

b(tape) a

.-1n
/tape_drive device:/p                   " find next real entry
s/^.*: //                               " isolate name of device
\b(add_build)                           " put in build buffer
-1n /^.*Model *= */n                    " skip to Model
s//-pattr model=/                       " make attribute from it
\b(add_build)                           " put in build buffer
-1n /^.*Tracks *= */n                   " skip to Tracks
s//,track=/                             " make attribute from it
\b(add_build)                           " put in build buffer
-1n /^.*Densities *= */n                " skip to Densities
s// /
s/ /,den=/
\b(add_build)                           " put in build buffer
-1n /^.*Speed *= */n                    " skip to Speed
s//,speed=/                             " make attribute from it
\b(add_build)                           " put in build buffer

b(build)                                " go into build buffer and insert
1i
register_resource tape_drive
\c\f                                    " get out of insert mode
2m(name)                                " put the name in a buffer
i\c\b(name)\c\f                         " put it back
$a
-owner system
-pacc system_low:system_high
-acc system_low:system_high
-acs_path >sc1>rcp>\c\b(name)\c\f       " acquire it to system
w build_temp
e file_output register.ec
e string [contents build_temp]
e revert_output
1,$d                                    " clear the buffer
r register.ec
$s/ *,/,/                                " make the attributes adjacent
w
1,$d                                    " so build can be reused
b(0)                                    " go back to main buffer
\c\b(tape)\f                            " and try to iterate (recur)

b(0)                                    " get back to main buffer

" process disk drives

/Device type: "disk_drive"/n            " find disk section

" set up buffer program to create registration command from each disk entry

b(disk) a

.-1n
/disk_drive device:/p                   " find next real entry
s/^.*: //                               " isolate name of device
\b(add_build)                           " put in build buffer
-1n /^.*Model *= */n                    " skip to Model
s//-pattr model=/                       " make attribute from it
\b(add_build)                           " put in build buffer

b(build)                                " go into build buffer and insert
1i
register_resource disk_drive
\c\f                                    " get out of insert, and append to end
$a
,use=io,use=ss
\c\f                                    " get out of append
2m(name)                                " put the name in a buffer
i\c\b(name)\c\f                         " put it back
$a
-owner system
-pacc system_low:system_high
-acc system_low:system_high
-acs_path >sc1>rcp>\c\b(name)\c\f       " acquire it to system
w build_temp
e fo register.ec
e string [contents build_temp]
e ro
1,$d                                    " clear the buffer
r register.ec
$s/ *,/,/                                " make the attributes adjacent
w
1,$d
b(0)
\c\b(disk)\f                            " and try to iterate (recur)

b(0)                                    " get back to main buffer

" process console

/Device type: "console"/n               " find console section

" set up buffer program to create registration command from each console entry

b(console) a

.-1n
/console device:/p                      " find next real entry
s/^.*: //                               " isolate name of device
\b(add_build)                           " put in build buffer
-1n /^.*Model *= */n                   " skip to Model
s//-pattr model=/                       " make attribute from it
\b(add_build)                           " put in build buffer

b(build)                                " go into build buffer and insert
1i
register_resource console
\c\f                                    " get out of insert
2m(name)                                " put the name in a buffer
i\c\b(name)\c\f                         " put it back
$a
-owner system
-pacc system_low:system_high
-acc system_low:system_high
-acs_path >sc1>rcp>\c\b(name)\c\f       " acquire it to system
w build_temp
e fo register.ec
e string [contents build_temp]
e ro
1,$d                                    " clear the buffer
b(0)                                    " go back to main buffer
\c\b(console)\f                         " and try to iterate (recur)

b(0)                                    " get back to main buffer

" process printer

/Device type: "printer"/n               " find printer section

" set up buffer program to create registration command from each printer entry

b(printer) a

.-1n
/printer device:/p                      " find next real entry
s/^.*: //                               " isolate name of device
\b(add_build)                           " put in build buffer
-1n /^.*Model *= */n                    " skip to Model
s//-pattr model=/                       " make attribute from it

b(test)                                 " have to do funnies to state speed
a
s/model=1200/&,speed=1200/
\c\f
b(0)
\c\b(test)

b(test)                                 " and again...
1i
s/model=1600/&,speed=1600/
\c\f
b(0)
\c\b(test)

b(test)                                 " and again...
1i
s/model=301/&,speed=1150/
\c\f
b(0)
\c\b(test)

b(test)
1,$d                                    " leave buffer clean for next iter.
b(0)

\b(add_build)                           " put in build buffer

b(build)                                " go into build buffer and insert
1i
register_resource printer
\c\f                                    " get out of insert
2m(name)                                " put the name in a buffer
i\c\b(name)\c\f                         " put it back
$a
-owner system
-pacc system_low:system_high
-acc system_low:system_high
-acs_path >sc1>rcp>\c\b(name)\c\f       " acquire it to system
w build_temp
e fo register.ec
e string [contents build_temp]
e ro
1,$d                                    " clear the buffer
b(0)                                    " go back to main buffer
\c\b(printer)\f                         " and try to iterate (recur)

b(0)                                    " get back to main buffer

" process punch

/Device type: "punch"/n                 " find punch section

" set up buffer program to create registration command from each punch entry

b(punch) a

.-1n
/punch device:/p                        " find next real entry
s/^.*: //                               " isolate name of device
\b(add_build)                           " put in build buffer
-1n /^.*Model *= */n                    " skip to Model
s//-pattr model=/                       " make attribute from it
\b(add_build)                           " put in build buffer

b(build)                                " go into build buffer and insert
1i
register_resource punch
\c\f                                    " get out of insert
2m(name)                                " put the name in a buffer
i\c\b(name)\c\f                         " put it back
$a
-owner system
-pacc system_low:system_high
-acc system_low:system_high
-acs_path >sc1>rcp>\c\b(name)\c\f       " acquire it to system
w build_temp
e fo register.ec
e string [contents build_temp]
e ro
1,$d                                    " clear the buffer
b(0)                                    " go back to main buffer
\c\b(punch)\f                           " and try to iterate (recur)

b(0)                                    " get back to main buffer

" process reader

/Device type: "reader"/n                " find reader section

" set up buffer program to create registration command from each reader entry

b(reader) a

.-1n
/reader device:/p                       " find next real entry
s/^.*: //                               " isolate name of device
\b(add_build)                           " put in build buffer
-1n /^.*Model *= */n                    " skip to Model
s//-pattr model=/                       " make attribute from it
\b(add_build)                           " put in build buffer

b(build)                                " go into build buffer and insert
1i
register_resource reader
\c\f                                    " get out of insert
2m(name)                                " put the name in a buffer
i\c\b(name)\c\f                         " put it back
$a
-owner system
-pacc system_low:system_high
-acc system_low:system_high
-acs_path >sc1>rcp>\c\b(name)\c\f       " acquire it to system
w build_temp
e fo register.ec
e string [contents build_temp]
e ro
1,$d                                    " clear the buffer
b(0)                                    " go back to main buffer
\c\b(reader)\f                          " and try to iterate (recur)

b(0)                                    " get back to main buffer

" call the subroutines we just created.

b(0)                                    " go back to main buffer
\b(tape)
\b(clear_build)
\b(disk)
\b(clear_build)
\b(console)
\b(clear_build)
\b(printer)
\b(clear_build)
\b(punch)
\b(clear_build)
\b(reader)
\b(clear_build)

" sort this exec_com in place so it will look pretty.

e sort_seg register.ec -replace

b(last)
r register.ec
1,$s/$/\c
/
w

e delete build_temp rcp.list

" And we are done.  Quit, anticipating the query about modified buffers.

qf
