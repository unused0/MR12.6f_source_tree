/* ***********************************************************
   *                                                         *
   *                                                         *
   * Copyright, (C) Honeywell Information Systems Inc., 1981 *
   *                                                         *
   *                                                         *
   *********************************************************** */



/* HISTORY COMMENTS:
  1) change(86-02-27,Blair), approve(86-02-27,MCR7358),
     audit(86-04-23,RBarstad), install(86-05-28,MR12.0-1062):
     85-03-27 JG Backs: Added three new pl1 modules to the order statement:
     xmail_Review_Processing_, xmail_Review_Outgoing_, xmail_Review_Printing_.
     
     85-04-19 JG Backs: Added xmail_im_mgr_ module to the order statement.
  2) change(86-02-27,Blair), approve(86-02-27,MCR7358),
     audit(86-04-23,RBarstad), install(86-05-28,MR12.0-1062):
     Add the new pl1 module xmail_rebuild_value_seg_ to the order statement.
  3) change(86-03-21,Blair), approve(86-03-21,MCR7358),
     audit(86-04-23,RBarstad), install(86-05-28,MR12.0-1062):
     Add the cds segment xmail_data_ to be used to hold the pathname of the
     directory of internal info segs.
                                                   END HISTORY COMMENTS */


/* Bindfile for: bound_executive_mail_ */

/* Created:  October 1981 by S. Krupp 

   Modified: October 1983 by D. Schimke to remove xmail_archive_msgs_,
   xmail_get_line_ and xmail_send_mail_file_msg_ Also moved PNOTICE_xmail
   to first entry in bound segment as it should be.

   84-07-28 Davids: Added the xmail_write_msgs_ module to the order statement.

   84-09-27 JG Backs: Added xmail_default_fkeys module to the order statement.

   84-11-08 JG Backs: Added a trailing underscore to xmail_default_fkeys_
   module for consistancy.  Audit change.

*/

Objectname:        bound_executive_mail_;

  Order: PNOTICE_xmail,
         xmail,
         xmail_default_fkeys_,
         xmail_multics_mode_,
         xmail_display_menu_,
         xmail_delete_msgs_,
         xmail_file_msgs_,
         xmail_get_choice_,
         xmail_select_msgs_,
         xmail_dir_manager_,
         xmail_err_,
         xmail_window_manager_,
         xmail_review_defers_,
         xmail_delete_dreply_,
         xmail_Review_Mlist_,
         xmail_send_stored_msg_,
         xmail_Mail_File_Maint_,
         xmail_display_help_,
         xmail_emacs_ext_main_,
         xmail_emacs_ext_mlist_,
         xmail_Review_Defaults_,
         xmail_sw_,
         xmail_Executive_Mail_,
         xmail_Consult_Files_,
         xmail_create_mlist_,
         xmail_update_mlist_,
         xmail_prepare_msg_,
         xmail_forward_msg_,
         xmail_reply_msg_,
         xmail_select_file_,
         xmail_Process_Mail_,
         xmail_send_msg_,
         xmail_discard_file_,
         xmail_undelete_msgs_,
         xmail_list_msgs_,
         xmail_dprint_msgs_,
         xmail_display_msgs_,
         xmail_write_msgs_,
         xmail_error_,
         xmail_dyn_menu_,
         xmail_get_dyn_choice_,
         xmail_Send_Mail_,
         xmail_redisplay_,
         xmail_Getting_Started_,
         xmail_get_str_,
         xmail_print_,
         xmail_rebuild_value_seg_,
         xmail_delete_mlist_,
         xmail_send_mail_print_msg_,
         xmail_process_user_msg_,
         xmail_value_,
         xmail_validate_,
         xmail_dprint_mlist_,
         xmail_display_mlist_,
         xmail_create_menu_,
         xmail_im_mgr_,
         xmail_Review_Printing_,
         xmail_Review_Outgoing_,
         xmail_Review_Processing_,
         xmail_area_,
         xmail_data_;


   Global:         delete;

   Addname:        xmail,
                   executive_mail,
	         xmail_emacs_ext_main_,
	         xmail_emacs_ext_mlist_,
	         xmail_err_;

   No_Table;

/* Instructions for individual components */

objectname:        PNOTICE_xmail;

objectname:        xmail;

   synonym:        executive_mail;

   retain:         xmail,executive_mail;

objectname:        xmail_emacs_ext_main_;

   global:         retain;

objectname:        xmail_emacs_ext_mlist_;

   global:         retain;

objectname:        xmail_err_;

   global:         retain;

/* END bound_executive_mail_.bind */
