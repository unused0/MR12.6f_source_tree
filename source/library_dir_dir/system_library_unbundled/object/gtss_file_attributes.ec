&version 2
&- ***********************************************************
&- *                                                         *
&- * Copyright, (C) Honeywell Information Systems Inc., 1983 *
&- *                                                         *
&- ***********************************************************
&- Author:    Ron Barstad	1982
&- Modified:  Ron Barstad	1982	Added mode parameter
&- Modified:  Ron Barstad	83-04-18	Changed max size to 20 times current
&trace &command off
&trace &input off
&if [exists argument &1] &then &goto cont
string USAGE: ec &ec_name GCOS_FILE_NAME {mode}
&quit

&label cont
&if [exists branch &1] &then &goto cont2
string &ec_name.ec: Segment &1 unknown. Quiting.
&quit

&label cont2
&goto &ec_name

&label query_init
&label attr
add_name &1 &1.mode.[response "mode: r or s?"]
add_name &1 &1.maxl.[response "maxl: #llinks?"]
add_name &1 &1.curl.[response "curl: #llinks?"]
add_name &1 &1.attr.[response "attr: 12octal?"]
add_name &1 &1.busy.[response "busy: yes or no?"]
add_name &1 &1.null.[response "null: yes or no?"]
add_name &1 &1.noal.[response "noal: #allocations?"]
add_name &1 &1.crdt.[response "crdt: MMDDYY?"]
&quit

&label init
&label gcos_file_attributes
&label gtss_file_attributes
&label gfa
&default &undefined s
&if &[equal &[search &2 rs] 1] &then &goto add_n
string &ec_name.ec: Mode "&r2" unknown, must be r or s. Quiting.
&quit

&label add_n
&set llinks [ceil [quotient [times [status &1 -cl] 1024] 320]]
add_name &1 &1.mode.[substr &2 1 1]
add_name &1 &1.maxl.[times &(llinks) 20]
add_name &1 &1.curl.&(llinks)
add_name &1 &1.attr.0
add_name &1 &1.busy.no
add_name &1 &1.null.no
add_name &1 &1.noal.0
add_name &1 &1.crdt.[substr [date] 1 2][substr [date] 4 2][substr [date] 7 2]
&quit
