/* ***********************************************************
   *                                                         *
   *                                                         *
   * Copyright, (C) Honeywell Information Systems Inc., 1981 *
   * Copyright, (C) Honeywell Information Systems Inc., 1980 *
   *                                                         *
   *                                                         *
   *********************************************************** */

/* compose subroutine to measure string lengths in millipoints */

/* format: style2,ind2,ll79,dclind4,idind15,comcol41,linecom */

comp_measure_:
  proc (a_str, afnt_ptr, fill, art, quad, a_lmeas, meas1_ptr, meas2_ptr,
       info_ptr);

/* PARAMETERS */

    dcl a_str	   char (1020) var; /* string to be measured - IN */
    dcl afnt_ptr	   ptr;		/* font description - IN */
    dcl fill	   bit (1);	/* 0 = simple measuring - IN */
				/* 1 = measuring filled text */
    dcl art	   bit (1);	/* 1 = line may have art - IN */
    dcl quad	   bit (6);	/* line set control */
    dcl a_lmeas	   fixed bin (31);	/* measure of target line - IN */
				/* if a_lmeas is 0, then then entire */
				/* string is to be measured */
    dcl meas1_ptr	   ptr;		/* str that fits - IN */
    dcl meas2_ptr	   ptr;		/* overflow str - IN */
    dcl info_ptr	   ptr;		/* info structure for str - IN */

    dcl 1 afnt	   aligned like fntstk_entry based (afnt_ptr);
				/* string that fits */
    dcl 1 meas1	   aligned like text_entry.cur based (meas1_ptr);
				/* overflow string */
    dcl 1 meas2	   aligned like text_entry.cur based (meas2_ptr);

/* LOCAL STORAGE */

    dcl art_str_ptr	   ptr;		/* art symbol overlay */
    dcl art_str	   char (3) unal based (art_str_ptr);
				/* artwork strings */
    dcl 1 art_xcep	   static options (constant),
	2 str	   (50) char (6)
		   init ("/oo/", "/ss/", "/cc/", "/==/", "|--|", "|**|", "-~~-", "~**~",
		   "SvvS", "svvs", "s""""s", "S^^S", "s^^s", "S""""S", "HvvH", "hvvh",
		   "h""""h", "H^^H", "h^^h", "H""""H", "0^^0", "1^^1", "2^^2", "3^^3",
		   "4^^4", "5^^5", "6^^6", "7^^7", "8^^8", "9^^9", "0vv0", "1vv1", "2vv2",
		   "3vv3", "4vv5", "5vv5", "6vv6", "7vv7", "8vv8", "9vv9", "0^^0", "1^^1",
		   "2^^2", "3^^3", "4^^5", "5^^5", "6^^6", "7^^7", "8^^8", "9^^9"),
	2 code	   (50) char (1)
		   init ("o", "s", "c", "=", "`", " ", "f", " ",
		   (42) (1)"");
    dcl art_xcep_ndx   fixed bin;	/* index of art_str in xcep list */
    dcl avg1	   fixed bin (31);	/* accumulated average wordspace */
    dcl avgwsp	   fixed bin (31);	/* current average wordspace */
    dcl brkrw	   fixed bin (31);	/* width of word breaker */
    dcl chrct1	   fixed bin;
    dcl ctl_width	   fixed bin (31);	/* width of device ctl string */
    dcl debug_sw	   bit (1);	/* effective debug switch */
    dcl detail_sw	   bit (1);	/* effective detail debug switch */
    dcl ENwidth	   fixed bin (31);	/* width of EN */
    dcl gap_ahead	   fixed bin;
    dcl gap_found	   bit (1);
    dcl gaps1	   fixed bin;
    dcl hyphenated	   bit (1);
    dcl iscn	   fixed bin;	/* string scanning index */
    dcl (jj, k)	   fixed bin;	/* working index */
    dcl 1 lfnt	   aligned like fntstk_entry;
    dcl lmeas	   fixed bin (31);
    dcl max1	   fixed bin (31);	/* accumulated maximum wordspace */
    dcl maxwsp	   fixed bin (31);	/* current maximum wordspace */
    dcl min1	   fixed bin (31);	/* accumulated minimum wordspace */
    dcl minwsp	   fixed bin (31);	/* current minimum wordspace */
    dcl mptstrk	   fixed bin (31);	/* mpt -> strokes conversion */
    dcl oflo	   bit (1);	/* line overflow switch */
    dcl PSwidth	   fixed bin (31);	/* width of PS */
    dcl runits	   fixed bin;	/* rel_units in current font */
    dcl str	   char (1020) var; /* working string */
    dcl strlen	   fixed bin;
    dcl tchar	   char (1);	/* for debugging */
    dcl trnsw	   bit (1);	/* 1= translation is active */
    dcl true_size	   fixed bin (31);	/* true point size */
    dcl width1	   fixed bin (31);	/* string width accumulator */
    dcl word	   char (1020) var; /* measured word */
    dcl wordct	   fixed bin;	/* chars in word */
    dcl wrdstrt	   fixed bin;	/* start of word in string */
    dcl wordu	   fixed bin (31);	/* word size */
    dcl wrdbrkr	   char (1);

    dcl (addr, bin, dec, divide, index, length, ltrim, max, min, null, rank,
        reverse, round, rtrim, search, substr, unspec, verify)
		   builtin;

    strlen = length (a_str);		/* copy args */
    str = a_str;			/**/
				/* set debug switches */
    debug_sw = (shared.bug_mode & db_sw);
    detail_sw = (debug_sw & dt_sw);

    if debug_sw
    then call ioa_ ("measure: (^d/^d w^f g^d ^f^2(/^f^) m^f ^a ^f"
	    || "^[ F^; ^^F^]^[ A^; ^^A^])^/^5x""^a^vx""", meas1.chrct,
	    strlen, show (meas1.width, 12000), meas1.gaps,
	    show (meas1.width + meas1.min, 12000),
	    show (meas1.width + meas1.avg, 12000),
	    show (meas1.width + meas1.max, 12000), show (a_lmeas, 12000),
	    afnt.name, show (afnt.size, 1000), fill, art,
	    comp_util_$display (a_str, 0, "0"b),
	    strlen - length (rtrim (a_str)));

    lfnt, meas1.font = afnt;		/* starting font for string */
    fnttbl_ptr = fnttbldata.ptr (lfnt.devfnt);
    if fill
    then wrdbrkr = shared.wrd_brkr;
    else wrdbrkr = "";

    if siztbl.ct = 1		/* validate size */
    then true_size = siztbl.size (1);
    else true_size = afnt.size;

    runits = fnttbl.rel_units;	/* conversion values in current font */
    mptstrk = divide (true_size, runits, 31, 0);

    minwsp = fnttbl.min_wsp;		/* wordspace values */
    avgwsp = fnttbl.avg_wsp;
    maxwsp = fnttbl.max_wsp;		/**/
				/* other useful widths */
    ENwidth = fnttbl.units (rank (EN));
    PSwidth = fnttbl.units (rank (PS));

    wrdstrt, wordu, ctl_width, gap_ahead = 0;

    word = "";			/* initialize local values */
    unspec (meas2), gap_found, hyphenated, oflo = "0"b;
    meas2.font = afnt;
    trnsw = (length (shared.trans.in) > 0);

    lmeas = a_lmeas;
    if lmeas ^= 0
    then lmeas = divide (lmeas, mptstrk, 31, 0);
    width1 = meas1.width;

    chrct1 = meas1.chrct;
    gaps1 = meas1.gaps;
    if width1 ^= 0
    then width1 = divide (width1, mptstrk, 31, 0);
    min1 = meas1.min;
    if min1 ^= 0
    then min1 = divide (min1, mptstrk, 31, 0);
    avg1 = meas1.avg;
    if avg1 ^= 0
    then avg1 = divide (avg1, mptstrk, 31, 0);
    max1 = meas1.max;
    if max1 ^= 0
    then max1 = divide (max1, mptstrk, 31, 0);

scan_loop:			/* scan the given string */
    do iscn = chrct1 + 1 by 0 while (iscn <= strlen);

font_char:
      do;
        word = "";
        wordct, wordu = 0;
        wrdstrt = iscn;

        if iscn <= strlen
        then
font_char_loop:
	do iscn = iscn to strlen;	/* take all font chars */

(nostrg):
next_tchar:
	  tchar = substr (str, iscn, 1);
	  if tchar = DC1		/* device control string? */
	  then
ctl_char:
	    do;			/**/
	      if wordu > 0		/* any text pending? */
	      then
	        do;
		if detail_sw
		then call ioa_ ("^[^-^6x^]^4d,^2d ""^a"" ^f "
			|| "(g^d ^f = ^f/^f/^f)", ^gap_found, wrdstrt,
			wordct, comp_util_$display (word, 0, "0"b),
			show (wordu * mptstrk, 12000), gaps1,
			show ((width1 + wordu) * mptstrk, 12000),
			show ((width1 + wordu + min1) * mptstrk, 12000),
			show ((width1 + wordu + avg1) * mptstrk, 12000),
			show ((width1 + wordu + max1) * mptstrk, 12000));

/*		width1 = width1 + wordu;
/*		wordu = 0;*/
		gap_found = "0"b;
	        end;		/**/
				/* set control pointer */
	      DCxx_p = addr (substr (str, iscn, 1));
				/* nothing more for waits */
	      if dcfs.type = type_wait
	      then ;

	      if dcfs.type = type_font/* is it a font change? */
	      then
	        do;		/* really changing? */
		if lfnt.devfnt ^= dcfs.f
		then
		  do;
		    lfnt.devfnt = dcfs.f;
		    fnttbl_ptr = fnttbldata.ptr (lfnt.devfnt);
		    lfnt.name = fnttbl.name;
				/* new rel units? */
		    if runits ^= fnttbl.rel_units
		    then
		      do;
		        runits = fnttbl.rel_units;
		        mptstrk = divide (true_size, runits, 31, 0);
		        minwsp = divide (fnttbl.min_wsp, mptstrk, 31, 0);
		        avgwsp = divide (fnttbl.avg_wsp, mptstrk, 31, 0);
		        maxwsp = divide (fnttbl.max_wsp, mptstrk, 31, 0);
		        ENwidth = fnttbl.units (rank (EN));
		        PSwidth = fnttbl.units (rank (PS));
		      end;
		  end;		/**/
				/* changing size? */
		if lfnt.size ^= dcfs.p
		then
		  do;
		    lfnt.size = dcfs.p;
				/* revalidate size */
		    if siztbl.ct = 1
		    then true_size = siztbl.size (1);
		    else true_size = lfnt.size;
		  end;
	        end;

	      else
	        do;		/* must be shift or plot */
		if dcxx.Xctl = "01"b/* if an X value, account for it */
		then ctl_width = divide (dcshort_val.v1, mptstrk, 31, 0);
		else if dcxx.Xctl = "10"b
		then ctl_width = divide (dclong_val.v1, mptstrk, 31, 0);
		else ctl_width = 0; /* clear control width */
	        end;

	      if lmeas > 0		/* dont do more than we have to */
	      then if width1 + ctl_width > lmeas
		 then goto return_;

	      wordu = wordu + ctl_width;
	      wordct = wordct + dcxx.leng + 3;
	      word = word || substr (str, iscn, dcxx.leng + 3);

	      if detail_sw
	      then call ioa_ ("^[^-^6x^]^4d,^2d ""^a"" ^f "
		      || "(g^d ^f = ^f/^f/^f)", ^gap_found, wrdstrt,
		      wordct, comp_util_$display (word, 0, "0"b),
		      show (wordu * mptstrk, 12000), gaps1,
		      show ((width1 + wordu) * mptstrk, 12000),
		      show ((width1 + wordu + min1) * mptstrk, 12000),
		      show ((width1 + wordu + avg1) * mptstrk, 12000),
		      show ((width1 + wordu + max1) * mptstrk, 12000));

	      gap_found = "0"b;	/* skip over control string */
	      iscn = iscn + dcxx.leng + 3;

	      if iscn <= strlen
	      then goto next_tchar;
	      else goto EOL_check;
	    end ctl_char;

	  if art			/* check for line art symbols */
	  then
art_:
	    do;
	      art_str_ptr = addr (substr (str, iscn));

	      if iscn < strlen - 1	/* dont check EOL garbage */
	      then if substr (art_str, 2, 1) = BSP
		 then
		   do;
		     art_xcep_ndx = index (string (art_xcep.str), art_str);
				/* find it? */
		     if art_xcep_ndx > 0
		     then
		       do;
		         art_xcep_ndx =
			    divide (art_xcep_ndx + 5, 6, 17, 0);

		         wordu = wordu
			    + fnttbl
			    .units (rank (art_xcep.code (art_xcep_ndx)));
		         word = word || substr (str, iscn, 3);
		         wordct = wordct + 3;
				/* step over it */
		         iscn = iscn + 3;

		         if iscn > strlen
		         then
			 do;
			   if detail_sw
			   then call ioa_ ("^[^-^6x^]^4d,^2d ""^a"" ^f "
				   || "(g^d ^f = ^f/^f/^f)",
				   ^gap_found,
				   wrdstrt - bin (word = ""), wordct,
				   comp_util_$display (word, 0, "0"b),
				   show (wordu * mptstrk, 12000),
				   gaps1,
				   show ((width1 + wordu) * mptstrk,
				   12000),
				   show ((width1 + wordu + min1)
				   * mptstrk, 12000),
				   show ((width1 + wordu + avg1)
				   * mptstrk, 12000),
				   show ((width1 + wordu + max1)
				   * mptstrk, 12000));

			   width1 = width1 + wordu;
			   chrct1 = chrct1 + wordct + gap_ahead;
			   meas1.font = lfnt;
			   gap_found = "0"b;
			   gap_ahead, wordu = 0;

			   goto return_;
			 end;

		         else goto next_tchar;
		       end;
		   end;
	    end art_;

	  if trnsw
	  then
	    do;
	      k = index (shared.trans.in, tchar);
	      if k > 0
	      then tchar = substr (shared.trans.out, k, 1);
	    end;			/**/
				/* if a word break */
	  if tchar = " " | tchar = wrdbrkr & wrdbrkr ^= ""
	  then
	    do;
EOL_check:
	      if detail_sw
	      then call ioa_ ("^[^-^6x^]^4d,^2d ""^a"" ^f "
		      || "(g^d ^f = ^f/^f/^f)", ^gap_found,
		      wrdstrt - bin (word = ""), wordct,
		      comp_util_$display (word, 0, "0"b),
		      show (wordu * mptstrk, 12000), gaps1,
		      show ((width1 + wordu) * mptstrk, 12000),
		      show ((width1 + wordu + min1) * mptstrk, 12000),
		      show ((width1 + wordu + avg1) * mptstrk, 12000),
		      show ((width1 + wordu + max1) * mptstrk, 12000));
				/* if word does not overset */
				/* or no measure was given */
	      if width1 + wordu + avg1 <= lmeas | quad = just
/****		 & (width1 + wordu + avg1 > lmeas*/
		 & width1 + wordu + min1 <= lmeas | lmeas <= 0
	      then
	        do;		/* stay up to date */
		width1 = width1 + wordu;
		chrct1 = chrct1 + wordct + gap_ahead;
		meas1.font = lfnt;
		gap_found = "0"b;
		gap_ahead, wordu, wordct = 0;
	        end;

	      else		/* this word oversets */
	        do;
		if shared.hyph_mode & ^hyphenated
		     & (quad = just & width1 + max1 - maxwsp <= lmeas
		     | quad ^= just & width1 + avg1 - avgwsp <= lmeas)
		then if try_hyph ()
		     then
		       do;
		         hyphenated = "1"b;
		         iscn = wrdstrt;
		         goto font_char;
		       end;

		wrdbrkr = shared.wrd_brkr;

		if chrct1 > 0 & gap_ahead > 0
		then
		  do;
		    gaps1 = gaps1 - 1;
		    min1 = min1 - minwsp;
		    avg1 = avg1 - avgwsp;
		    max1 = max1 - maxwsp;
		  end;

		if detail_sw
		then call ioa_$nnl ("^5xOFLO");

		oflo = "1"b;
		goto return_;
	        end;

	      if iscn < strlen	/* if not EOL */
				/* or measuring all */
		 | (iscn = strlen & lmeas = 0)
	      then
word_break:
	        do;
		if tchar ^= " "	/* if a word breaker */
		then
breaker:
		  do;
		    word = word || tchar;
		    wordct = wordct + 1;
		    brkrw = fnttbl.units (rank (tchar));

		    if detail_sw
		    then call ioa_ ("^[^-^6x^]^4d,^2d ""^a"" ^f "
			    || "(g^d ^f = ^f/^f/^f)", ^gap_found,
			    wrdstrt - bin (word = ""), wordct,
			    comp_util_$display (word, 0, "0"b),
			    show ((wordu + brkrw) * mptstrk, 12000),
			    gaps1,
			    show ((width1 + brkrw) * mptstrk, 12000),
			    show ((width1 + brkrw + min1) * mptstrk,
			    12000),
			    show ((width1 + brkrw + avg1) * mptstrk,
			    12000),
			    show ((width1 + brkrw + max1) * mptstrk,
			    12000));

		    iscn = iscn + 1;/* and count the breaker */
				/* does it all still fit? */
		    if width1 + max (min1, 0) + brkrw <= lmeas
		    then
		      do;		/* update first return data */
		        width1 = width1 + brkrw;
		        chrct1 = iscn - 1;
		        meas1.font = lfnt;
		        gap_ahead = 0;
		        goto font_char;
		      end;

		    else		/* this is the overset "word" */
		      do;
		        if shared.hyph_mode & ^hyphenated & lmeas > 0
			   & (quad = just
			   & width1 + max1 - maxwsp <= lmeas
			   | quad ^= just
			   & width1 + avg1 - avgwsp <= lmeas)
		        then if try_hyph ()
			   then
			     do;
			       hyphenated = "1"b;
			       iscn = wrdstrt;
			       goto font_char;
			     end;

		        wrdbrkr = shared.wrd_brkr;
		        oflo = "1"b;
		        goto return_;
		      end;	/**/
				/* does it all still fit? */
		    if width1 + avg1 + avgwsp + wordu <= lmeas
		         | quad = just & width1 + wordu + min1 <= lmeas
		    then
		      do;		/* update first return data */
		        width1 = width1 + wordu;
		        chrct1 = iscn - 1;
		        meas1.font = lfnt;
		        goto font_char;
		      end;

		    else		/* this is the overset word */
		      do;
		        if shared.hyph_mode & ^hyphenated
			   & (quad = just
			   & width1 + max1 - maxwsp <= lmeas
			   | quad ^= just
			   & width1 + avg1 - avgwsp <= lmeas)
		        then if try_hyph ()
			   then
			     do;
			       hyphenated = "1"b;
			       iscn = wrdstrt;
			       goto font_char;
			     end;

		        wrdbrkr = shared.wrd_brkr;
		        oflo = "1"b;
		        goto return_;
		      end;
		  end breaker;

		else		/* its a wordspace */
wrdspc:
		  do;
		    if fill	/* preserve punctuation space */
		         & search (reverse (word), ".:!?") = 1
		         | search (reverse (word), """)") = 1
		         & search (reverse (word), ".!?") = 2
		    then if width1 + avg1 + PSwidth <= lmeas
			    | quad = just
			    & width1 + min1 + PSwidth <= lmeas
		         then
punct:
			 do;	/* add PS to the measured string */
(nostrg):
			   str = substr (str, 1, iscn - 1) || PS
			        || substr (str, iscn);
			   strlen = strlen + 1;
				/* take some width */
			   width1 = width1 + PSwidth;
			   chrct1 = chrct1 + 1;

			   if detail_sw
			   then call ioa_ ("^-^6x^4d, 1 ""^a"" ^f "
				   || "(g^d ^f = ^f/^f/^f)", iscn,
				   comp_util_$display ((PS), 0, "0"b),
				   show (PSwidth * mptstrk, 12000),
				   gaps1,
				   show (width1 * mptstrk, 12000),
				   show ((width1 + min1) * mptstrk,
				   12000),
				   show ((width1 + avg1) * mptstrk,
				   12000),
				   show ((width1 + max1) * mptstrk,
				   12000));
				/* step over it */
			   iscn = iscn + 1;
			 end punct;

(nostrg):
		    jj = verify (substr (str, iscn), " ") - 1;
		    if jj < 0
		    then jj = strlen - iscn + 1;

		    if fill
		    then
		      do;
		        gap_found = "1"b;
		        gap_ahead = 1;

		        if jj > 1	/* cast out all multiple blanks */
		        then
			do;
(nostrg, nostrz):
			  str = substr (str, 1, iscn - 1) || " "
			       || ltrim (substr (str, iscn));
			  strlen = length (str);
			end;	/**/
				/* this isnt EOL */
		        if iscn <= strlen
		        then
			do;
			  gaps1 = gaps1 + 1;
			  min1 = min1 + minwsp;
			  avg1 = avg1 + avgwsp;
			  max1 = max1 + maxwsp;
				/* end of undent field? */
			  if width1 >= lmeas & lmeas > 0 & ^fill
			  then goto return_;
			end;

		        if detail_sw & gaps1 > 0
		        then call ioa_$nnl ("^4d gap ^2d ^f", iscn, gaps1,
			        show (avgwsp * mptstrk, 12000));

		        iscn = iscn + 1;
		      end;

		    else if art
		    then
		      do;
		        str = substr (str, 1, iscn - 1) || copy (EN, jj)
			   || ltrim (substr (str, iscn));

		        width1 = width1 + jj * ENwidth;
		        chrct1 = chrct1 + jj;

		        if detail_sw
		        then call ioa_$nnl ("^4d ^2d EN^[s^;^x^] ^f", iscn,
			        jj, (jj > 1), show (jj * ENwidth, 12000))
			        ;
		        iscn = iscn + jj;
		        gap_found = "1"b;
		      end;

		    else
		      do;
		        width1 = width1 + jj * ENwidth;
		        chrct1 = chrct1 + jj;

		        if detail_sw
		        then call ioa_$nnl ("^4d ^2d SP^[s^;^x^] ^f", iscn,
			        jj, (jj > 1),
			        show (jj * ENwidth * mptstrk, 12000));
		        iscn = iscn + jj;
		        gap_found = "1"b;
		      end;

		    goto font_char;
		  end wrdspc;
	        end word_break;
	      else goto return_;
	    end;

	  else
	    do;
	      if tchar = PS		/* punctuation space? */
	      then
	        do;		/* does the word fit? */
		if width1 + wordu + avg1 <= lmeas
		     | quad = just & width1 + wordu + min1 <= lmeas
		then
		  do;		/* if PS does not overset */
				/* take the units for PS */
		    if width1 + avg1 + PSwidth < lmeas
		         | quad = just & width1 + PSwidth + min1 <= lmeas
		    then
		      do;
		        wordu = wordu + fnttbl.units (rank (tchar));
		        word = word || PS;
		        wordct = wordct + 1;
		      end;

		    else
		      do;		/* throw the PS away */
		        substr (str, iscn) = substr (str, iscn + 1);
		        strlen = strlen - 1;
				/* discard 1 char for loop control */
		        iscn = iscn - 1;
		      end;
		  end;

		else if fill	/* this is the overset word */
		then
		  do;		/* set return data */
		    if shared.hyph_mode & ^hyphenated
		         & (quad = just & width1 + max1 - maxwsp <= lmeas
		         | quad ^= just & width1 + avg1 - avgwsp <= lmeas)
		    then if try_hyph ()
		         then
			 do;
			   hyphenated = "1"b;
			   iscn = wrdstrt;
			   goto font_char;
			 end;

		    wrdbrkr = shared.wrd_brkr;
		    oflo = "1"b;
		    goto return_;
		  end;
	        end;		/**/
				/* NOT punctuation space */
	      else if tchar ^= DC1
	      then
	        do;
		wordu = wordu + fnttbl.units (rank (tchar));
		word = word || tchar;
		wordct = wordct + 1;
	        end;
	    end;
	end font_char_loop;
      end font_char;
end_scan_loop:
    end scan_loop;			/**/
				/* fell out, must be EOL */
    if wordu > 0			/* any leftovers? */
    then goto EOL_check;

return_:
    if chrct1 > 0			/* strip trailing PS */
    then if substr (str, chrct1, 1) = PS
         then
	 do;
	   if chrct1 = strlen
	   then str = substr (str, 1, chrct1 - 1);
	   else
(nostrg):
	     str = substr (str, 1, chrct1 - 1) || substr (str, chrct1 + 1);
	   width1 = width1 - PSwidth;
	   chrct1 = chrct1 - 1;
	   meas2.chrct = meas2.chrct - 1;
	   strlen = strlen - 1;
	 end;

    meas1.chrct = min (chrct1, strlen);
    meas1.gaps = max (gaps1, 0);
    meas1.avg = max (avg1 * mptstrk, 0);
    meas1.min = max (min1 * mptstrk, 0);
    meas1.max = max (max1 * mptstrk, 0);
    meas1.width = max (width1 * mptstrk, 0);

    if ^oflo
    then meas2.font = meas1.font;

    else
      do;
        meas2.font = lfnt;
        meas2.chrct = min (iscn - bin (iscn < strlen), strlen);
        meas2.gaps = gaps1 + gap_ahead;
        meas2.width = (width1 + wordu) * mptstrk;
        meas2.min = (min1 + minwsp) * mptstrk;
        meas2.avg = (avg1 + avgwsp) * mptstrk;
        meas2.max = (max1 + maxwsp) * mptstrk;
      end;

    a_str = str;			/* store back possibly modified str */

    if debug_sw
    then
      do;
        if detail_sw & (^fill & chrct1 = strlen)
	   | (fill & chrct1 = length (rtrim (a_str)))
        then call ioa_$nnl ("^5xEOL");

        iscn = chrct1 - length (rtrim (substr (str, 1, chrct1)));
        call ioa_ ("^/^5x(measure: [1=^d/^d^2( ^f^)^2(/^f^)] "
	   || "[2=^d/^d^2( ^f^)^2(/^f^)] ^a ^f)^[^/^-""^a^vx""^]",
	   meas1.chrct, meas1.gaps, show (meas1.width, 12000),
	   show (meas1.width + meas1.min, 12000),
	   show (meas1.width + meas1.avg, 12000),
	   show (meas1.width + meas1.max, 12000), meas2.chrct, meas2.gaps,
	   show (meas2.width, 12000), show (meas2.width + meas2.min, 12000),
	   show (meas2.width + meas2.avg, 12000),
	   show (meas2.width + meas2.max, 12000), meas2.font.name,
	   show (meas2.font.size, 1000), (meas1.chrct > 0),
	   comp_util_$display (substr (str, 1, meas1.chrct), 0, "0"b), iscn);

        if meas2.chrct > 0
        then
	do;
	  call ioa_ ("^-""^a""",
	       comp_util_$display (
	       substr (str, meas1.chrct + 1, meas2.chrct - meas1.chrct), 0,
	       "0"b));
	end;
      end;

    return;
%page;
hyph_wrd:
  proc (hword, hspace, hpoint, ercd);

    dcl hword	   char (*);
    dcl hspace	   fixed bin;
    dcl hpoint	   fixed bin;
    dcl ercd	   fixed bin (35);

    dcl (i, j)	   fixed bin;
    dcl space	   fixed bin;

    dcl hyphenate_word_
		   entry (char (*), fixed bin, fixed bin, fixed bin (35));

    ercd = 0;			/* preset output values */
    hpoint = 0;			/**/
				/* check impossible cases */
    if hspace < 2 | hspace > length (hword)
    then return;
    else space = hspace;

    if shared.hwrd_data_ptr ^= null	/* is hword in the hwrd list */
    then
      do;
        do j = 1 to hwrd_data.count while (hwrd_data.word (j) ^= hword);
        end;

        if j <= hwrd_data.count	/* yes */
        then
	do;			/* try hyphenation first */
	  i = index (reverse (substr (hwrd_data.hpts (j), 1, space - 1)),
	       "1"b);
	  if i ^= 0
	  then i = space - i;
	  if i < length (hword)
	  then if substr (hword, i + 1, 1) = "-"
	       then i = i + 1;

	  if i = 0		/* no hyphenation point */
	  then
	    do;
	      i = index (reverse (hwrd_data.brkpts (j)), "1"b);
	    end;

	  hpoint = i;
	end;

        else call hyphenate_word_ (hword, space, hpoint, ercd);

      end;

    else call hyphenate_word_ (hword, space, hpoint, ercd);

  end hyph_wrd;
%page;
show:
  proc (datum, scale) returns (fixed dec (11, 3));
    dcl datum	   fixed bin (31);
    dcl scale	   fixed bin (31);
    dcl retval	   fixed dec (11, 3);

    retval = round (dec (round (divide (datum, scale, 31, 11), 10), 11, 4), 3);
    return (retval);
  end show;
%page;
try_hyph:
  proc returns (bit (1));

    dcl ercd	   fixed bin (35);
    dcl hp_space	   fixed bin (31);	/* width of hyph punct */
    dcl hpoint	   fixed bin;
    dcl hscndx	   fixed bin;	/* word scan index */
    dcl hspace	   fixed bin;	/* line chars left for hyphenation */
    dcl hword	   char (strlen - chrct1 + 2) var;
    dcl hwrdl	   fixed bin static;/* length of trial hyphenation word */
    dcl i		   fixed bin;

    hscndx = wrdstrt;
    hwrdl = iscn - wrdstrt;

trim_trailing:			/* any trailing punctuation? */
    if search (reverse (substr (str, hscndx, hwrdl)), ".,;:!?""" || wrdbrkr)
         = 1
    then
      do;
        hwrdl = hwrdl - 1;
        goto trim_trailing;
      end;			/**/
				/* line space left (in chars) */
    if quad = just
    then hspace =
	    divide ((lmeas - width1 - min1) * mptstrk, shared.EN_width, 17,
	    0);
    else hspace =
	    divide ((lmeas - width1 - avg1) * mptstrk, shared.EN_width, 17,
	    0);

    hp_space = 0;			/* check leading punctuation */
trim_leading:
    if index ("/([{""" || PAD || wrdbrkr, substr (str, hscndx, 1)) ^= 0
    then
      do;
        hp_space = hp_space +		/* calculate its width */
	   fnttbl.units (rank (substr (str, hscndx, 1)));
        hscndx = hscndx + 1;		/* step over it */
        hspace = hspace - 1;		/* 1 fewer hyph chars */
        hwrdl = hwrdl - 1;
        goto trim_leading;
      end;

    i = -1			/* anything embedded? */
         +
         verify (reverse (substr (str, hscndx, hwrdl)),
         "/.;:!?,()[]{}""" || PAD || PS);
    if i > 0
    then
      do;
        hwrdl = hwrdl - i;		/* to force hyphenation */
        hspace = min (hspace, hwrdl - 1);
      end;

    if hspace > shared.hyph_size	/* enough space? */
    then
      do;
        hpoint = 0;			/* clear hyph point index */
        hword = substr (str, hscndx, hwrdl);

        if shared.bug_mode
        then call ioa_$nnl ("^5x(hyph: ^d ^d ""^a""", hspace, hwrdl, hword);

        call hyph_wrd (substr (str, hscndx, hwrdl), hspace, hpoint, ercd);
        if ercd ^= 0
        then goto hyph_err;

        if hpoint = 0		/* try simple plurals */
        then
	do;
	  if index (substr (hword, hwrdl), "s") = 1
	  then
	    do;
	      call hyph_wrd (substr (hword, 1, hwrdl - 1), hspace, hpoint,
		 ercd);
	      if ercd ^= 0
	      then goto hyph_err;
	    end;

	  if hpoint = 0
	  then
	    do;
	      if index (substr (hword, hwrdl), "es") = 1
	      then
	        do;
		call hyph_wrd (substr (hword, 2, hwrdl - 2), hspace,
		     hpoint, ercd);
		if ercd ^= 0
		then goto hyph_err;
	        end;

	      if hpoint = 0
	      then
	        do;
		if index (substr (hword, hwrdl), "ies") = 1
		then
		  do;
		    call hyph_wrd (substr (hword, 3, hwrdl - 3) || "y",
		         hspace, hpoint, ercd);
		    if ercd ^= 0
		    then
hyph_err:
		      call comp_report_$ctlstr (2, ercd, text.input.info,
			 str,
			 "Error returned by hyphenate_word_ subroutine.")
			 ;
		    return ("0"b);
		  end;
	        end;
	    end;
	end;

        if shared.bug_mode
        then call ioa_ (" @ ^d)", hpoint);

hyph_it:
        if hpoint >= shared.hyph_size
        then
	do;
	  hpoint = hscndx + hpoint - 1;

	  if substr (str, hpoint, 1) ^= "-"
	  then
	    do;			/* insert the "-" */
	      str = substr (str, 1, hpoint) || "-"
		 || substr (str, hpoint + 1);
	      strlen = strlen + length ("-");
	      hpoint = hpoint + length ("-");
				/* adjust modified text */
	      if strlen <= text.input.mod_start
	      then text.input.mod_start =
		      text.input.mod_start + length ("-");
	      else if strlen <= text.input.mod_start + text.input.mod_len - 1
	      then text.input.mod_len = text.input.mod_len + length ("-");
	    end;

	  wrdbrkr = "-";		/* set hyphen as word breaker */
	  gap_found = "0"b;
	  return ("1"b);
	end;

        if hpoint = 0
        then
	do;			/**/
				/* unbreakable word? */
	  if width1 = 0 & wordu > divide (text.parms.measure, mptstrk, 31, 0)
	  then
	    do;
	      call comp_report_$ctlstr (2, 0, info_ptr, ctl_line,
		 "Text too long for output line.");

/*	      meas1 = meas2;
/*	      strlen = meas2.chrct;
/*	      text.input.quad = quadl;*/

/*	      call put_line;
/*	      text.input.quad = just;*/

/*	      goto fill_loop;*/
	      return ("0"b);
	    end;
	end;

      end;

    return ("0"b);

  end try_hyph;
%page;
alln:
  entry;
    db_sw, dt_sw = "1"b;
    return;
allf:
  entry;
    db_sw, dt_sw = "0"b;
    return;
dbn:
  entry;
    db_sw = "1"b;
    return;
dbf:
  entry;
    db_sw = "0"b;
    dt_sw = "0"b;
    return;
    dcl db_sw	   bit (1) static init ("0"b);

dtn:
  entry;
    db_sw = "1"b;
    dt_sw = "1"b;
    return;
dtf:
  entry;
    dt_sw = "0"b;
    return;
    dcl dt_sw	   bit (1) static init ("0"b);
%page;
%include comp_DCdata;
%include comp_dvt;
%include comp_entries;
%include comp_fntstk;
%include comp_font;
%include comp_hwrd_data;
%include comp_metacodes;
%include comp_shared;
%include comp_text;
%include comp_tree;
%include compstat;

  end comp_measure_;
