/* ***********************************************************
   *                                                         *
   *                                                         *
   * Copyright, (C) Honeywell Information Systems Inc., 1981 *
   * Copyright, (C) Honeywell Information Systems Inc., 1980 *
   *                                                         *
   *                                                         *
   *********************************************************** */

/* compose subroutine to assign values to tree symbols */

/* format: style2,ind3,ll80,dclind4,idind15,comcol41,linecom */

comp_update_symbol_:
   proc (priv, stack, defsw, symbol, buffer);

/* PARAMETERS */

      dcl priv	     bit (1);	/* (IN) 1= OK to set internal vars */
      dcl stack	     bit (1);	/* (IN) 1= stack variable */
      dcl defsw	     bit (1);	/* (IN) 1= define only, no value */
      dcl symbol	     char (32);	/* (IN) symbol to be updated */
      dcl buffer	     char (*) var;	/* (IN) symbol value string */

/* LOCAL STORAGE */

      dcl bufndx	     fixed bin (21);/* buffer scan index */
      dcl code	     fixed bin (35);/* error code */
      dcl CREATE	     bit (1) static options (constant) init ("1"b);
      dcl exptyp	     fixed bin;	/* type of <expr> */
      dcl LOG	     fixed bin static options (constant) init (1);
      dcl logval	     bit (1);	/* <expr> value from control line */
      dcl needtyp	     fixed bin;	/* <expr> type needed */
      dcl NUM	     fixed bin static options (constant) init (2);
      dcl numval	     fixed bin (31);/* <expr> value from control line */
      dcl STR	     fixed bin static options (constant) init (3);
      dcl val_attr	     bit (9);	/* attributes of <expr> */
      dcl val_key	     char (5) var;	/* value keyword for debug */
      dcl val_scale	     fixed bin (31);/* value scale for debug */
      dcl val_len	     fixed bin init (0);
				/* length of string */
      dcl value	     char (1020) var;
				/* <expr> value from buffer */

      dcl (after, addr, before, dec, divide, index, length, null, round, substr)
		     builtin;

      needtyp = 0;

      if shared.bug_mode | dt_sw
      then call ioa_ ("update_symbol: (^a^[ P^]^[ S^] ""^a"")", symbol, priv,
	      stack, comp_util_$display (buffer, 0, "0"b));
				/* search tree for this symbol */
      call comp_util_$search_tree (symbol, CREATE);
				/* a builtin? */
      if tree.areandx = 1 & tree.entryndx <= const.builtin_count
      then
         do;
	  if ^(priv | symbol = "DotAddLetter" | symbol = "PageBlock")
	  then
	     do;
	        call comp_report_ (2, 0,
		   "Invalid attempt to set a builtin variable "
		   || "with a set-reference control.", addr (ctl.info),
		   ctl_line);
	        return;
	     end;
	  if symbol = "DotAddLetter"
	  then needtyp = STR;
	  if symbol = "PageBlock"
	  then needtyp = LOG;
         end;

      if stack
      then call comp_util_$push (symbol);

      if defsw
      then
         do;
	  if shared.bug_mode | dt_sw
	  then call ioa_ ("^5x(update_symbol: ^d|^d ^a DEFINED)",
		  tree.areandx, tree.entryndx, symbol);
	  return;
         end;

      tree_var_ptr = tree.var_ptr (tree.areandx);

      if index (ctl_line, ".src") = 1	/* if its a counter set */
      then
         do;			/* make it a counting variable */
	  tree_var.flags (tree.entryndx) = counter_attr | numeric_attr;
				/* delta given? */
	  if index (buffer, "by") ^= 0
	  then
	     do;
	        call comp_expr_eval_ (before (buffer, "by"), 1,
		   addr (ctl.info), NUM, 0, "0"b,
		   tree_var.num_loc (tree.entryndx) -> num_value, "",
		   val_attr, 0);
	        call comp_expr_eval_ (after (buffer, "by"), 1,
		   addr (ctl.info), NUM, 0, "0"b,
		   tree_var.incr_loc (tree.entryndx) -> num_value, "", ""b,
		   0);
	     end;

	  else call comp_expr_eval_ (buffer, 1, addr (ctl.info), NUM, 0, "0"b,
		  tree_var.num_loc (tree.entryndx) -> num_value, "",
		  val_attr, 0);

	  tree_var.flags (tree.entryndx) =
	       tree_var.flags (tree.entryndx) | val_attr;

	  if shared.bug_mode | dt_sw
	  then call ioa_ ("^5x(update_symbol: ^d|^d ^a counter/^a ^f,^f)",
		  tree.areandx, tree.entryndx, symbol,
		  substr (mode_string,
		  2 * tree_var.mode (tree.entryndx) + 1, 2),
		  tree_var.num_loc (tree.entryndx) -> num_value,
		  tree_var.incr_loc (tree.entryndx) -> num_value);

	  return;
         end;

      if symbol = "Args" | buffer = "" | index (symbol, "CommandArg") = 1
      then
         do;
	  value = buffer;
	  goto str_vrbl;
         end;

      bufndx = 1;			/* evaluate the buffer */
      call comp_expr_eval_ (buffer, bufndx, addr (ctl.info), needtyp, exptyp,
	 logval, numval, value, val_attr, code);
      if code ^= 0
      then return;

      if bufndx > 0 & bufndx < length (buffer)
      then
         do;
	  if substr (buffer, bufndx, 1) = ")"
	  then call comp_report_ (2, 0, "Extra right parenthesis",
		  addr (ctl.info), ctl_line);
	  else call comp_report_ (2, 0, "Improper expression",
		  addr (ctl.info), ctl_line);
         end;

      if exptyp = STR
      then
         do;
str_vrbl:				/* if not stacked */
	  if ^substr (tree_var.flags (tree.entryndx), 9, 1)
	  then
	     do;			/* set to string type */
	        tree_var.flags (tree.entryndx) = string_attr;
				/* set value */
	        tree_var.str_loc (tree.entryndx) -> txtstr = value;
	     end;

	  else
	     do;			/* stacked */
	        tree_var.flags (tree.entryndx) = string_attr | push_attr;
				/* pushed string */
	        tree_var.num_loc (tree.entryndx) -> stack_box.txtstr = value;
	     end;

	  if shared.bug_mode | dt_sw
	  then call ioa_ ("^5x(update_symbol: ^d|^d ^a string (^d) ""^a"")",
		  tree.areandx, tree.entryndx, symbol, length (value),
		  comp_util_$display (value, 0, "0"b));
         end;

      else if exptyp = LOG
      then
         do;
	  if tree.areandx > 1 | tree.entryndx > const.builtin_count
	  then tree_var.flags (tree.entryndx) = flag_attr;
	  tree_var.flag_loc (tree.entryndx) -> flag_value = logval;

	  if shared.bug_mode | dt_sw
	  then call ioa_ ("^5x(update_symbol: ^d|^d ^a flag ^[T^;F^])",
		  tree.areandx, tree.entryndx, symbol, logval);
         end;

      else			/* numeric */
         do;			/* if not stacked */
	  if ^substr (tree_var.flags (tree.entryndx), 9, 1)
	  then
	     do;
	        tree_var.num_loc (tree.entryndx) -> num_value = numval;
	        tree_var.flags (tree.entryndx) = val_attr;
	     end;

	  else
	     do;
	        tree_var.flags (tree.entryndx) = val_attr | push_attr;
	        tree_var.flags (tree.entryndx) =
		   tree_var.flags (tree.entryndx) | push_attr;
	        tree_var.num_loc (tree.entryndx) -> stack_box.numval = numval;
	     end;

	  if shared.bug_mode | dt_sw
	  then
	     do;

	        if bool (tree_var.flags (tree.entryndx), hspace_attr, "0001"b)
		   ^= "0"b
	        then
		 do;
		    val_key = "hspc";
		    val_scale = 12000;
		 end;

	        else
		 do;
		    val_key = "unscl";
		    val_scale = 1000;
		 end;

	        call ioa_ ("^5x(update_symbol: ^d|^d ^a numeric(^a)/^a ^f)",
		   tree.areandx, tree.entryndx, symbol, val_key,
		   substr (mode_string,
		   2 * tree_var.mode (tree.entryndx) + 1, 2),
		   round (
		   dec (round (divide (numval, val_scale, 31, 11), 10), 11,
		   4), 3));
	     end;
         end;

      return;

      dcl dt_sw	     bit (1) static init ("0"b);
dtn:
   entry;
      dt_sw = "1"b;
      return;
dtf:
   entry;
      dt_sw = "0"b;
      return;
%page;
%include comp_entries;
%include comp_fntstk;
%include comp_page;
%include comp_shared;
%include comp_stack_box;
%include comp_text;
%include comp_tree;
%include comp_varattrs;
%include compstat;

   end comp_update_symbol_;
