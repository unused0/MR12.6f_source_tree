/* ***********************************************************
   *                                                         *
   *                                                         *
   * Copyright, (C) Honeywell Information Systems Inc., 1981 *
   *                                                         *
   *                                                         *
   *********************************************************** */

/*	compose subroutine to initialize the internal data base */

/* format: style2,ind2,ll79,dclind4,idind15,comcol41,linecom */

comp_init_:
  proc;
    return;			/* no entry here */

/* LOCAL STORAGE */

    dcl date_time_string
		   char (16);	/* current date and time */
    dcl debug_sw	   bit (1);	/* effective debug switch */
    dcl ercd	   fixed bin (35);	/* system error code */

    dcl (addr, min, null, size)
		   builtin;
    dcl cleanup	   condition;

    dcl clock_	   entry returns (fixed bin (71));
    dcl com_err_	   entry options (variable);
    dcl date_time_	   entry (fixed bin (71), char (*));
    dcl translator_temp_$get_segment
		   entry (char (*) aligned, ptr, fixed bin (35));

/* CONSTANT STATIC STORAGE */

    dcl builtin_names  (80) char (32) static options (constant)
		   init ("AlignMode", "ArgCount", "Args", "ArtMode",
		   "BlockIndex", "BlockName", "CallingFileName",
		   "CallingLineNo", "ChangeBar", "CommandArgCount", "Date",
		   "Device", "DeviceClass", "DeviceName", "DotAddLetter",
		   "Eqcnt", "EqMode", "ExtraMargin", "FileName",
		   "FillMode", "FirstPass", "FontName", "Footcnt",
		   "FootnoteMode", "FootReset", "From", "FrontPage",
		   "Galley", "HeadSpace", "Hyphenating", "Indent",
		   "IndentRight", "InputDirName", "InputFileName",
		   "InputLineno", "InsertIndex", "KeepMode",
		   "LineNumberOpt", "LineInput", "LinesLeft", "LineSpace",
		   "Measure", "NextPageNo", "OutputFileOpt", "PageBlock",
		   "PageCount", "PageLength", "PageLine", "PageNo",
		   "PageSpace", "PageWidth", "Parameter", "ParamPresent",
		   "Pass", "PictureCount", "PointSize", "Print", "StopOpt",
		   "SymbolDelimiter", "TableMode", "TextDepth",
		   "TextWidth", "Time", "TitleDelimiter", "To", "TrTable",
		   "Undent", "UndentRight", "UserInput", "VMargBottom",
		   "VMargFooter", "VMargHeader", "VMargTop", "WaitOpt",
		   "Widow", "WidowFoot",
				/**/
		   (4) (32)"");	/* set spares to PADs */

    dcl 1 initial_page_parms		/* initial page formatting parms */
		   aligned static options (constant),
	2 init_page_depth
		   fixed bin (31),	/* initial page depth */
	2 length	   fixed bin (31) init (792000),
				/* page length */
	2 lmarg,			/* page left margins */
	  3 even	   fixed bin (31) init (0),
	  3 odd	   fixed bin (31) init (0),
	2 margin,			/* margin values */
	  3 top	   fixed bin (31),
	  3 header   fixed bin (31),
	  3 footer   fixed bin (31),
	  3 bottom   fixed bin (31),
	2 measure	   fixed bin (31),	/* line space available for text */
	2 net,			/* net usable space on page */
	  3 even	   fixed bin (31),	/* even pages */
	  3 odd	   fixed bin (31),	/* odd pages */
	2 cols,			/* columns defined for the page */
				/* 1= balance columns */
	  3 bal	   bit (1) unal init ("1"b),
	  3 MBZ	   bit (17) unal init ((17)"0"b),
				/* the number of columns */
	  3 count	   fixed bin unal init (0);
				/* force following structure to even */
    dcl dummy	   ptr init (null ()) static options (constant);

    dcl 1 initial_shared		/* initial shared data values */
		   aligned static options (constant),
	2 version	   fixed bin (35),	/* version of this structure */
	2 chars,			/**/
	( 3 sym_delim		/* delimiter for symbols */
		   init ("%"),
	  3 ttl_delim		/* delimiter for title parts */
		   init ("|"),
	  3 wrd_brkr init (" ")	/* word break character */
	  )	   char (1) unal,
	  3 PAD	   char (1) unal init (""),
	2 cbar_type		/* change bar type */
		   char (4) var init (""),
	2 dot_add_letter		/* dot page add letter (= PAD) */
		   char (1) var init (""),
				/* width of EN in current font */
	2 EN_width   fixed bin (31) init (0),
				/* equation reference counter */
	2 eqn_refct  fixed bin init (1),
	2 footref_fcs		/* footnote ref FCS string */
		   char (8) init (""),
				/* footnote reset mode */
	2 ftn_reset  char (8) var init ("paged"),
				/* footnote ref counter */
	2 ftnrefct   fixed bin init (1),
				/* least word part size for hyphing */
	2 hyph_size  fixed bin (31) init (3),
	2 if_nest,		/* if/then/else logic control */
				/* depth of logic nest */
	  3 ndx	   fixed bin init (0),
	  3 e	   (25),		/* nest entries */
				/* .if control switch */
	    4 sw	   fixed bin,	/* 0=off, 1=(then), -1=(else) */
	    4 info   like text_entry.info,
	    4 line   char (256) var,	/* the control line */
	2 indctl,			/* indent ctls stack */
				/* current level */
	  3 ndx	   fixed bin init (0),
				/* switch bits */
	  3 stk	   (0:35) bit (1) unal init ((36) (1)"0"b),
	2 input_dirname		/* dir containing current input file */
		   char (168) var init (""),
	2 input_filename		/* current input file name */
		   char (200) var init (""),
	2 lead			/* current linespacing value */
		   fixed bin (31) init (0),
	2 lit_count  fixed bin (35) init (0),
				/* count of literal lines */
	2 next_pagenmbr		/* next page number */
		   char (32) var init ("1"),
	2 output_file		/* output file identifier */
		   char (32) var init (""),
	2 pagecount		/* number of pages produced */
		   fixed bin init (0),
	2 pagenum,		/* page number structure */
				/* level currently counting */
	  3 index	   fixed bin init (1),
				/* separator chars (NULs) */
	  3 sep	   (20) char (1) unal init ((20) (1)" "),
				/* the counters */
	  3 nmbr	   (20) fixed bin (31) init ((20) 0),
				/* display modes */
	  3 mode	   (20) fixed bin (8) unal init ((20) 0),
				/* command line parameter */
	2 parameter  char (254) var init (""),
				/* passed parameter flag */
	2 param_pres bit (1) unal init ("0"b),
	2 pass_counter
		   fixed bin init (1),
				/* pass counter */
	2 picture,		/* picture blocks */
	  3 count	   fixed bin init (0),
	  3 space			/* total picture space */
		   fixed bin (31) init (0),
	  3 blk	   (10),		/* picture blocks */
	    4 type		/* type = page/col */
		   char (4) init ((10) (0)""),
	    4 place		/* place = top/cen/bot */
		   char (4) init ((10) (0)""),
	    4 ptr			/* pointer to block */
		   ptr init ((10) null),
	    4 size		/* size of picture */
		   fixed bin (31) init ((10) 0),
	2 ptrs,
	( 3 aux_file_data_ptr,	/* -> auxiliary file data */
	  3 blank_footer_ptr,	/* -> blank page footer */
	  3 blank_header_ptr,	/* -> blank page header */
	  3 blank_text_ptr,		/* -> blank page text */
	  3 blkptr,		/* -> active text */
	  3 colptr,		/* current column */
	  3 compout_ptr,		/* iocb pointer for output */
	  3 compx_ptr,		/* iocb pointer for compx file */
	  3 ctb_ptr,		/* current line artwork table */
	  3 epftrptr,		/* even page footer block */
	  3 ephdrptr,		/* even page header block */
	  3 fcb_ptr,		/* input file control block pointer */
	  3 ftnblk_data_ptr,	/* footnote block data */
	  3 footnote_header_ptr,	/* footnote header */
	  3 graphic_page_ptr,	/* graphic output page */
	  3 hit_data_ptr,		/* hit data pointer */
	  3 htab_ptr,		/* horizontal tab tables */
	  3 hwrd_data_ptr,		/* local hyphenation table */
	  3 insert_ptr,		/* data entry for current file */
	  3 opftrptr,		/* odd page footer block */
	  3 ophdrptr,		/* odd page header block */
	  3 ptb_ptr,		/* previous line artwork table */
	  3 spcl_blkptr,		/* "special" block pointer */
	  3 tbldata_ptr,		/* table column data */
	  3 tblkdata_ptr,		/* text block data array */
	  3 text_header_ptr		/* empty text header structure */
	  )	   ptr init (null),
	2 scale,			/* space conversion scale factors */
	  3 horz	   fixed bin (31) init (7200),
				/* horizontal */
	  3 vert	   fixed bin (31) init (12000),
				/* vertical */
	2 source_filename		/* source file name */
		   char (200) var init (""),
	2 sws,			/* switch bits */
	( 3 bug_mode init ("0"b),	/* debug mode */
	  3 compout_not_headed	/* compout not headed */
		   init ("1"b),
	  3 end_input		/* EOF for current input file */
		   init ("0"b),
	  3 end_output		/* 1 = no more output is wanted */
		   init ("0"b),
	  3 firstpass		/* 1 = first pass over input */
		   init ("1"b),
	  3 ftn_mode init ("0"b),	/* 1 = in footnote mode */
	  3 hyph_mode		/* hyphenating mode */
		   init ("0"b),
	  3 inserting_hfc		/* inserting hdr, ftr, or cap */
		   init ("0"b),
	  3 literal_mode		/* literal line mode flag */
		   init ("0"b),
	  3 pageblock		/* blocks belong to page */
		   init ("0"b),
	  3 picture_mode		/* building a picture */
		   init ("0"b),
	  3 print_flag		/* producing output */
		   init ("0"b),
	  3 purge_ftns		/* purging footnotes */
		   init ("0"b),
	  3 suppress_footref	/* supress number */
		   init ("0"b),
	  3 table_mode		/* 1 = in table mode */
		   init ("0"b)
	  )	   bit (1) unal,
	  3 MBZ	   bit (21) unal init ((21)"0"b),
	2 trans,			/* trans table for .tr */
	  3 in	   char (128) var init (""),
				/* input chars */
	  3 out	   char (128) var init (""),
				/* output chars */
	2 widow_size fixed bin (31) init (2),
				/* widow size */
	2 widow_foot fixed bin (31) init (1);
				/* widow size for footnotes */

    dcl 1 init_default_parms
		   aligned static options (constant),
	2 sws,			/* control switches */
				/* text alignment mode */
	  3 quad	   bit (6) unal init ("000001"b),
				/* artwork block flag */
	  3 art	   bit (1) unal init ("0"b),
	  3 cbar,			/* change bar flags */
				/* text addition flag */
	    4 add	   bit (1) unal init ("0"b),
				/* text deletion flag */
	    4 del	   bit (1) unal init ("0"b),
				/* text modification flag */
	    4 mod	   bit (1) unal init ("0"b),
	  3 fill_mode		/* fill mode */
		   bit (1) unal init ("1"b),
				/* block is a footnote */
	  3 footnote bit (1) unal init ("0"b),
				/* OBSOLETE */
	  3 hfc	   bit (1) unal init ("0"b),
	  3 htab_mode		/* horizontal tab mode flag */
		   bit (1) unal init ("0"b),
				/* keep mode */
	  3 keep	   bit (1) unal init ("0"b),
				/* block belongs to page */
	  3 page	   bit (1) unal init ("0"b),
	  3 title_mode		/* 1 = <title>s OK */
		   bit (1) unal init ("0"b),
	  3 MBZ	   bit (19) unal init ((19)"0"b),
	2 ftrptr	   ptr init (null), /* text caption block */
	2 cbar_level		/* change level for cbars */
		   char (1) aligned init (""),
	2 hdrptr	   ptr init (null), /* text header block */
	2 left,			/* left margin data */
	  3 indent   fixed bin (31) init (0),
	  3 undent   fixed bin (31) init (0),
				/* line spacing */
	2 linespace  fixed bin (31) init (12000),
				/* line space available for text */
	2 measure	   fixed bin (31) init (0),
	2 right	   like init_default_parms.left,
				/* arrays to the back of the bus, please! */
	2 fntstk,			/* stack of last 20 font changes */
				/* which one in use */
	  3 index	   fixed bin init (0),
	  3 entry	   (0:19) like fntstk_entry;
				/* the stack entries */
				/* empty text header structure */
    dcl 1 initial_text_header
		   aligned static options (constant),
	2 sws,			/* control switches */
	( 3 art	   init ("0"b),	/* block has artwork */
	  3 dfrftn   init ("0"b),	/* block is a deferred footnote */
	  3 modified init ("0"b),	/* block has modified lines */
	  3 no_trim  init ("0"b),	/* dont trim WS block */
	  3 oflo_ftn init ("0"b),	/* overflow footnote */
	  3 orphan   init ("0"b),	/* footnote is an orphan */
	  3 picture  init ("0"b),	/* picture block */
	  3 tblblk   init ("0"b),	/* a table block */
	  3 unref	   init ("0"b),	/* block is an unreffed footnote */
	  3 white	   init ("0"b)	/* block is a white space block */
	  )	   bit (1) unal,
	  3 MBZ	   bit (26) unal init ((26)"0"b),
				/* artwork line counter */
	2 art_count  fixed bin unal init (0),
	2 blkptr	   ptr init (null), /* pointer to suspended block */
				/* line count of text caption */
	2 cap_size   fixed bin unal init (0),
				/* size of text caption */
	2 cap_used   fixed bin (31) init (0),
				/* containing column */
	2 colno	   fixed bin unal init (0),
				/* line count for block */
	2 count	   fixed bin unal init (0),
				/* counter for equation lines */
	2 eqn_line_count
		   fixed bin unal init (0),
				/* OBSOLETE */
	2 first_text fixed bin unal init (0),
	2 ftn,			/* footnotes in the block */
				/* number */
	  3 ct	   fixed bin init (0),
				/* space needed */
	  3 usd	   fixed bin (31) init (0),
				/* block index values */
	  3 blkndx   (40) fixed bin init ((40) 0),
				/* line count of text header */
	2 head_size  fixed bin init (0),
				/* size of text header */
	2 head_used  fixed bin (31) init (0),
				/* block index of next output line */
	2 index	   fixed bin unal init (1),
				/* to count input keep lines */
	2 keep_count fixed bin unal init (0),
				/* last text line in column */
	2 last_line  fixed bin init (0),
				/* max title index value in block */
	2 mx_ttl_ndx fixed bin init (0),
				/* block name, if any */
	2 name	   char (32) var init (""),
	2 nofill_count		/* to count nofill lines */
		   fixed bin init (0),
	2 parms_ptr  ptr init (null), /* pointer to suspended parms */
				/* inter-unit reference */
	2 refer	   fixed bin init (0),
	2 refer_index		/* OBSOLETE */
		   fixed bin init (0),
	2 split			/* split point for balancing */
		   fixed bin init (0),
	2 trl_ws			/* trailing WS */
		   fixed bin (31) init (0),
	2 used			/* space used by a block */
		   fixed bin (31) init (0);
				/* initial symbol tree structure */
    dcl 1 init_tree_var		/* dimension MUST = MAX_TREE_AREA_CT */
		   (80) aligned static options (constant),
				/* type flags */
	2 flags	   bit (9) aligned init
				/**/
		   ("001001000"b,	/* AlignMode - string function */
		   "100010001"b,	/* ArgCount - stack numeric */
		   "001000001"b,	/* Args - stack string */
		   "000101000"b,	/* ArtMode - flag function*/
		   "100001000"b,	/* BlockIndex - binary function */
		   "001001000"b,	/* BlockName - string function */
		   "001001000"b,	/* CallingFileName - string function */
		   "100001000"b,	/* CallingLineNo - binary function */
		   "001000000"b,	/* ChangeBar - string */
		   "100000000"b,	/* CommandArgCount - binary */
		   "001000000"b,	/* Date - string */
		   "001000000"b,	/* Device - string */
		   "001001000"b,	/* DeviceClass - string function */
		   "001001000"b,	/* DeviceName - string function */
		   "001001000"b,	/* DotAddLetter - string function */
		   "100000000"b,	/* Eqcnt - binary */
		   "000101000"b,	/* EqMode - flag function */
		   "100000100"b,	/* ExtraMargin - horiz numeric */
		   "001000000"b,	/* FileName - string */
		   "000101000"b,	/* FillMode - flag function */
		   "000100000"b,	/* FirstPass - flag */
		   "001001000"b,	/* FontName - string function */
		   "100000000"b,	/* Footcnt - binary */
		   "000100000"b,	/* FootnoteMode - flag */
		   "001000000"b,	/* FootReset - string */
		   "001000000"b,	/* From - string */
		   "000101000"b,	/* FrontPage - flag function */
		   "000100000"b,	/* Galley - flag */
		   "100001010"b,	/* HeadSpace - vert numeric function */
		   "000100000"b,	/* Hyphenating - flag */
		   "100001100"b,	/* Indent - horiz num fcn */
		   "100001100"b,	/* IndentRight - horiz num fcn */
		   "001000000"b,	/* InputDirName - string */
		   "001000000"b,	/* InputFileName - string */
		   "100001000"b,	/* InputLineno - binary function */
		   "100000000"b,	/* InsertIndex - binary */
		   "000101000"b,	/* KeepMode - flag function */
		   "000100000"b,	/* LineNumberOpt - flag */
		   "001001000"b,	/* LineInput - string function */
		   "100001010"b,	/* LinesLeft - vert numeric function */
		   "100001010"b,	/* LineSpace - vert numeric function */
		   "100001100"b,	/* Measure - horiz numeric function */
		   "001001000"b,	/* NextPageNo - string function */
		   "000100000"b,	/* OutputFileOpt - flag */
		   "000100000"b,	/* PageBlock - flag */
		   "100001000"b,	/* PageCount - binary function */
		   "100001010"b,	/* PageLength - vert num function */
		   "100001010"b,	/* PageLine - vert num function */
		   "001001000"b,	/* PageNo - string function */
		   "100000000"b,	/* PageSpace - binary */
		   "100000100"b,	/* PageWidth - horiz numeric */
		   "001000000"b,	/* Parameter - string */
		   "000100000"b,	/* ParamPresent - flag */
		   "100000000"b,	/* Pass - binary */
		   "100000000"b,	/* PictureCount - binary */
		   "100011000"b,	/* PointSize - num function */
		   "000100000"b,	/* Print - flag */
		   "000100000"b,	/* StopOpt - flag */
		   "001001000"b,	/* SymbolDelimiter - string function */
		   "000100000"b,	/* TableMode - flag */
		   "100001010"b,	/* TextDepth - vert num function */
		   "100001100"b,	/* TextWidth - horiz num function */
		   "001000000"b,	/* Time - string */
		   "001001000"b,	/* TitleDelimiter - string function */
		   "001000000"b,	/* To - string */
		   "001001000"b,	/* TrTable - string function */
		   "100001100"b,	/* Undent - horiz num fcn */
		   "100001100"b,	/* UndentRight - horiz num fcn */
		   "001001000"b,	/* UserInput - string function */
		   "100001010"b,	/* VMargBottom - vert bin function */
		   "100001010"b,	/* VMargFooter - vert bin function */
		   "100001010"b,	/* VMargHeader - vert bin function */
		   "100001010"b,	/* VMargTop - vert numeric function */
		   "000100000"b,	/* WaitOpt - flag */
		   "100000000"b,	/* Widow - binary */
		   "100000000"b,	/* WidowFoot - binary */
		   (4) (9)"0"b),	/**/
				/* numeric display mode */
	2 mode	   fixed bin init ((80) 0),
	(
	2 flag_loc,		/* flag value pointer */
	2 num_loc,		/* num value pointer */
	2 incr_loc,		/* num increment pointer */
	2 str_loc			/* str value pointer */
	)	   ptr init ((80) null);

one:
  entry;				/* step 1 - enough to process args */
    if db_sw
    then call ioa_ ("init_$one:");	/**/
				/* pre-set all pointers */
    const.colhdrptr, const.call_stk_ptr, const.ctl_ptr, const.errblk_ptr,
         const.init_page_parms_ptr, const.default_parms_ptr,
         const.global_area_ptr, const.insert_data_ptr, const.local_area_ptr,
         const.option_ptr, const.page_ptr, const.page_header_ptr,
         const.page_parms_ptr, const.save_shared_ptr, const.shared_ptr,
         const.text_header_ptr, const.text_entry_ptr, const.fnttbldata_ptr,
         const.text_parms_ptr, const.tree_ptr, const.loctbl_ptr,
         const.outproc_ptr = null ();

/* initialize the constants */
    const.builtin_count = 80;		/* 76 + 4 spares */
				/* date of invocation */
    call date_time_ (clock_ (), date_time_string);
    const.date_value = before (date_time_string, "  ");
    const.null_str = "";		/* an empty string */
    const.time_value = after (date_time_string, "  ");
				/* global storage area */
    call translator_temp_$get_segment ("compose", const.global_area_ptr, ercd);
    if ercd ^= 0
    then
      do;
        call com_err_ (ercd, "compose", "Defining a global storage area.");
        signal cleanup;
        return;
      end;			/**/
				/* command options structure */
    const.option_ptr = allocate (const.global_area_ptr, size (option));
    unspec (option) = "0"b;		/* initialize them */
    option.version = option_version;
    option.cbar.level, option.pgc_select = " ";
    option.cbar.place = "o";
    option.cbar.space = 14400;
    option.cbar.left.mark, option.cbar.right.mark = modmark;
    option.cbar.del.mark = delmark;
    option.cbar.left.sep, option.cbar.right.sep, option.cbar.del.sep = 7200;
    option.cbar.left.width, option.cbar.right.width, option.cbar.del.width =
         7200;
    option.db_before_line, option.db_line_end, option.line_2 = 500000;
    option.db_file = " ";
    option.hyph_size = 3;
    option.linespace = 12000;
    option.pglst.from = "1";
    option.passes = 1;		/**/
				/* shared dynamic data */
    const.shared_ptr = allocate (const.global_area_ptr, size (shared));
    shared = initial_shared;		/* preset so cleanup doesn't barf */
    shared.version = shared_version;	/* version of shared structure */

    if db_sw
    then call ioa_ ("^5x(init_$one)");

    return;			/* end of step one */

two:
  entry;
    debug_sw = shared.bug_mode & db_sw;


    if debug_sw
    then call ioa_ ("init_$two:");	/* continue global allocations */
				/* control line */
    const.ctl_ptr = allocate (const.global_area_ptr, size (ctl));
				/* and buffer */
    ctl.ptr = allocate (const.global_area_ptr, size (ctl));
				/* default page formatting parms */
    const.init_page_parms_ptr =
         allocate (const.global_area_ptr, size (init_page_parms));
				/* default text formatting parms */
    const.default_parms_ptr =
         allocate (const.global_area_ptr, size (default_parms));
    default_parms = init_default_parms; /**/
				/* footnote formatting parms */
    const.footnote_parms_ptr =
         allocate (const.global_area_ptr, size (footnote_parms));
    footnote_parms = init_default_parms;/**/
				/* insert file data structure */
    const.insert_data_ptr =
         allocate (const.global_area_ptr, size (insert_data));
    insert_data.count, insert_data.index, insert_data.ref_area.count = 0;
				/* insert call stack */
    const.call_stk_ptr = allocate (const.global_area_ptr, size (call_stack));
				/* current page layout parms */
    const.page_parms_ptr = allocate (const.global_area_ptr, size (page_parms));
    shared.parameter = option.parameter;/* copy option data */
    shared.hyph_mode = option.hyph_opt; /**/
				/* current formatting parms */
    const.text_parms_ptr = allocate (const.global_area_ptr, size (text_parms));
    text_parms = init_default_parms;	/**/
				/* program variable symbol tree */
    const.tree_ptr = allocate (const.global_area_ptr, size (tree));
    tree.flag_ptr, tree.incr_ptr, tree.name_ptr, tree.num_ptr, tree.var_ptr =
         null;

    tree.count = 1;			/* storage for builtins */
    tree.flag_ptr (1) = allocate (const.global_area_ptr, size (tree_flags));
    tree.incr_ptr (1) = allocate (const.global_area_ptr, size (tree_incrs));
    tree.name_ptr (1) = allocate (const.global_area_ptr, size (tree_names));
    tree.num_ptr (1) = allocate (const.global_area_ptr, size (tree_nums));
    tree.var_ptr (1) = allocate (const.global_area_ptr, size (tree_var));
				/* output page structure */
    const.page_ptr = allocate (const.global_area_ptr, size (page));
    page.version = page_version;	/* and initialize */
				/* empty column header */
    const.colhdrptr = allocate (const.global_area_ptr, size (colhdr));
				/* empty text structures */
    const.text_entry_ptr = allocate (const.global_area_ptr, size (text_entry));
    const.text_header_ptr =
         allocate (const.global_area_ptr, size (text_header));
    text_header = initial_text_header;
    const.page_header_ptr =
         allocate (const.global_area_ptr, size (page_header));
    const.fnttbldata_ptr = allocate (const.global_area_ptr, size (fnttbldata));
    fnttbldata.ndx, fnttbldata.count = 0;
    fnttbldata.medsel_ptr = null;

    if debug_sw
    then call ioa_ ("^5x(init_$two)");

    return;			/* end of step two */

three:
  entry;
    debug_sw = shared.bug_mode & db_sw;

    if debug_sw
    then call ioa_ ("init_$three:");	/**/
				/* initialize variable tree */
    tree.count, tree.areandx = 1;	/* builtins go in the first area */
    tree.entry_ct (*) = 0;
    tree.entry_ct (1) = MAX_TREE_AREA_CT;
    tree_names_ptr = tree.name_ptr (1);
    string (tree_names) = string (builtin_names);
    tree_var_ptr = tree.var_ptr (1);
    tree_var = init_tree_var;		/**/
				/* AlignMode */
    tree_var.num_loc (1) = addr (tree.align_mode);
				/* ArgCount */
    stkbox_ptr = allocate (const.global_area_ptr, size (stack_box));
    stack_box = init_stack_box;
    tree_var.num_loc (2), tree_var.str_loc (2) = stkbox_ptr;
				/* Args */
    stkbox_ptr = allocate (const.global_area_ptr, size (stack_box));
    stack_box = init_stack_box;
    tree_var.num_loc (3), tree_var.str_loc (3) = stkbox_ptr;
				/* ArtMode */
    tree_var.num_loc (4) = addr (tree.art_mode);
				/* BlockIndex */
    tree_var.num_loc (5) = addr (tree.block_index);
				/* BlockName */
    tree_var.num_loc (6) = addr (tree.block_name);
				/* CallingFileName */
    tree_var.num_loc (7) = addr (tree.calling_file_name);
				/* CallingLineNo */
    tree_var.num_loc (8) = addr (tree.callers_lineno);
				/* ChangeBar */
    tree_var.str_loc (9) = addr (shared.cbar_type);
				/* CommandArgCount */
    tree_var.num_loc (10) = addr (option.arg_count);
				/* Date */
    tree_var.str_loc (11) = addr (const.date_value);
				/* Device */
    tree_var.str_loc (12) = addr (option.device);
				/* DeviceClass */
    tree_var.num_loc (13) = addr (tree.devclass);
				/* DeviceName */
    tree_var.num_loc (14) = addr (tree.devname);
				/* DotAddLetter - func */
    tree_var.num_loc (15) = addr (tree.dot_addltr);
				/* DotAddLetter - val */
    tree_var.str_loc (15) = addr (shared.dot_add_letter);
				/* Eqcnt */
    tree_var.num_loc (16) = addr (shared.eqn_refct);
				/* EqMode */
    tree_var.num_loc (17) = addr (tree.equation_mode);
				/* ExtraMargin */
    tree_var.num_loc (18) = addr (option.extra_indent);
				/* FileName */
    tree_var.str_loc (19) = addr (shared.source_filename);
				/* FillMode */
    tree_var.num_loc (20) = addr (tree.fill_mode);
				/* FirstPass */
    tree_var.flag_loc (21) = addr (shared.firstpass);
				/* FontName */
    tree_var.num_loc (22) = addr (tree.fontname);
				/* Footcnt */
    tree_var.num_loc (23) = addr (shared.ftnrefct);
				/* FootnoteMode */
    tree_var.flag_loc (24) = addr (shared.ftn_mode);
				/* FootReset */
    tree_var.str_loc (25) = addr (shared.ftn_reset);
				/* From */
    tree_var.str_loc (26) = addr (option.pglst (0).from);
				/* FrontPage */
    tree_var.num_loc (27) = addr (tree.frontpage);
				/* Galley */
    tree_var.flag_loc (28) = addr (option.galley_opt);
				/* HeadSpace */
    tree_var.num_loc (29) = addr (tree.head_space);
				/* Hyphenating */
    tree_var.flag_loc (30) = addr (shared.hyph_mode);
				/* Indent */
    tree_var.num_loc (31) = addr (tree.left_indent);
				/* IndentRight */
    tree_var.num_loc (32) = addr (tree.right_indent);
				/* InputDirName */
    tree_var.str_loc (33) = addr (shared.input_dirname);
				/* InputFileName */
    tree_var.str_loc (34) = addr (shared.input_filename);
				/* InputLineno */
    tree_var.num_loc (35) = addr (tree.text_lineno);
				/* InsertIndex */
    tree_var.num_loc (36) = addr (insert_data.index);
				/* KeepMode */
    tree_var.num_loc (37) = addr (tree.keep_mode);
				/* LineNumberOpt */
    tree_var.flag_loc (38) = addr (option.number_opt);
				/* LineInput */
    tree_var.num_loc (39) = addr (tree.line_input);
				/* LinesLeft */
    tree_var.num_loc (40) = addr (tree.linesleft);
				/* LineSpace */
    tree_var.num_loc (41) = addr (tree.linespace);
				/* Measure */
    tree_var.num_loc (42) = addr (tree.measure_bif);
				/* NextPageNo */
    tree_var.num_loc (43) = addr (tree.next_pageno);
				/* OutputFileOpt */
    tree_var.flag_loc (44) = addr (option.output_file_opt);
				/* PageBlock */
    tree_var.flag_loc (45) = addr (shared.pageblock);
				/* PageCount */
    tree_var.num_loc (46) = addr (tree.pagecount);
				/* PageLength */
    tree_var.num_loc (47) = addr (tree.page_length);
				/* PageLine */
    tree_var.num_loc (48) = addr (tree.pagelines);
				/* PageNo */
    tree_var.num_loc (49) = addr (tree.pageno);
    tree_var.num_loc (50) = null;	/* PageSpace */
				/* PageWidth */
    tree_var.num_loc (51) = addr (page_parms.measure);
				/* Parameter */
    tree_var.str_loc (52) = addr (shared.parameter);
				/* ParamPresent */
    tree_var.flag_loc (53) = addr (shared.param_pres);
				/* Pass */
    tree_var.num_loc (54) = addr (shared.pass_counter);
				/* PictureCount */
    tree_var.num_loc (55) = addr (shared.picture.space);
				/* PointSize */
    tree_var.num_loc (56) = addr (tree.pointsize);
				/* Print */
    tree_var.flag_loc (57) = addr (shared.print_flag);
				/* StopOpt */
    tree_var.flag_loc (58) = addr (option.stop_opt);
				/* SymbolDelimiter func */
    tree_var.num_loc (59) = addr (tree.symbol_delimiter);
				/* TableMode */
    tree_var.flag_loc (60) = addr (shared.table_mode);
				/* TextDepth */
    tree_var.num_loc (61) = addr (tree.text_depth);
				/* TextWidth */
    tree_var.num_loc (62) = addr (tree.text_width);
				/* Time */
    tree_var.str_loc (63) = addr (const.time_value);
				/* TitleDelimiter func */
    tree_var.num_loc (64) = addr (tree.title_delimiter);
				/* To */
    tree_var.str_loc (65) = addr (option.pglst (0).to);
				/* TrTable */
    tree_var.num_loc (66) = addr (tree.trans);
				/* Undent */
    tree_var.num_loc (67) = addr (tree.left_undent);
				/* UndentRight */
    tree_var.num_loc (68) = addr (tree.right_undent);
				/* UserInput */
    tree_var.num_loc (69) = addr (tree.userinput);
				/* VMargBottom */
    tree_var.num_loc (70) = addr (tree.bottom_margin);
				/* VMargFooter */
    tree_var.num_loc (71) = addr (tree.footer_margin);
				/* VMargHeader */
    tree_var.num_loc (72) = addr (tree.header_margin);
				/* VMargTop */
    tree_var.num_loc (73) = addr (tree.top_margin);
				/* WaitOpt */
    tree_var.flag_loc (74) = addr (option.wait_opt);
				/* Widow */
    tree_var.num_loc (75) = addr (shared.widow_size);
				/* WidowFoot */
    tree_var.num_loc (76) = addr (shared.widow_foot);
				/* page formatting parms */
    init_page_parms = initial_page_parms;
    if comp_dvt.pdl_max > 0
    then init_page_parms.length = min (792000, comp_dvt.pdl_max);
    init_page_parms.init_page_depth = comp_dvt.vmt_min;
    init_page_parms.margin.top = comp_dvt.def_vmt;
    init_page_parms.margin.header = comp_dvt.def_vmh;
    init_page_parms.margin.footer = comp_dvt.def_vmf;
    init_page_parms.margin.bottom = comp_dvt.def_vmb;
    init_page_parms.measure = min (comp_dvt.pdw_max, 468000);
    init_page_parms.net.even, init_page_parms.net.odd =
         init_page_parms.length - init_page_parms.margin.top
         - init_page_parms.margin.header - init_page_parms.margin.footer
         - init_page_parms.margin.bottom;
				/* page structure */
    page.image_ptr, page.column_ptr (*), page.col_image_ptr (*) = null ();
				/* local storage area */
    call translator_temp_$get_segment ("compose", const.local_area_ptr, ercd);
    if ercd ^= 0
    then
      do;
        call com_err_ (ercd, "compose", "Defining a local storage area.");
        signal cleanup;
        return;
      end;			/**/
				/* continue allocations */
    call_stack.count, call_stack.index = 0;
				/* call box for source file */
    call_stack.ptr (0) = allocate (const.local_area_ptr, size (call_box));
				/* column 0 structure */
    page.column_ptr (0) = allocate (const.local_area_ptr, size (col));
    col0.margin.left = 0;		/* set initial column parms */
    col0.margin.right, col0.parms.measure = 468000;
    col0.hdrptr, col0.ftrptr = null ();
    col0.hdrusd, col0.ftrusd = 0;
    col0.blkptr (*) = null ();	/* make sure block pointers are null */
				/* text block data */
    shared.tblkdata_ptr = allocate (const.local_area_ptr, size (tblkdata));
    unspec (tblkdata) = "0"b;
    tblkdata.block.ptr, tblkdata.line_area.ptr, tblkdata.text_area.ptr =
         null ();
    tblkdata.block.free, tblkdata.line_area.free, tblkdata.text_area.free =
         "1"b;

    if debug_sw
    then call ioa_ ("^5x(init_$three)");

    return;			/* end of step three */

    dcl db_sw	   bit (1) static init ("0"b);
				/* local debug swtich */
dbn:
  entry;
    db_sw = "1"b;
    return;
dbf:
  entry;
    db_sw = "0"b;
    return;
%page;
%include comp_column;
%include comp_DCcodes;
%include comp_dvt;
%include comp_fntstk;
%include comp_font;
%include comp_footnotes;
%include comp_insert;
%include comp_option;
%include comp_page;
%include comp_shared;
%include comp_stack_box;
%include comp_text;
%include comp_tree;
%include compstat;
%include translator_temp_alloc;

  end comp_init_;
