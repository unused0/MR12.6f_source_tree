/* ***********************************************************
   *                                                         *
   *                                                         *
   * Copyright, (C) Honeywell Information Systems Inc., 1981 *
   * Copyright, (C) Honeywell Information Systems Inc., 1980 *
   *                                                         *
   *                                                         *
   *********************************************************** */

/* compose subroutine to record input errors */

/* format: style2,ind3,ll80,dclind4,idind16,comcol41,linecom */

comp_report_:
   proc (a_sev, a_code, a_message, a_info_ptr, a_line);

/* PARAMETERS */

      dcl a_sev	      fixed bin;	/* error a_severity */
      dcl a_code	      fixed bin (35);
				/* system errcode; if any */
      dcl a_message	      char (*);	/* error message */
      dcl a_info_ptr      ptr;	/* line info structure */
      dcl a_line	      char (*) varying;
				/* text from offending line */

      dcl 1 line_info     like text_entry.info based (info_ptr);

/* LOCAL STORAGE */

      dcl call_nest	      char (4096) var init ("");
      dcl code	      fixed bin (35);
				/* error code */
      dcl ctl_str	      bit (1);	/* 1= ctlstr was called */
      dcl ercd	      fixed bin (35);
				/* local error code */
      dcl errlen	      fixed bin (35);
				/* length of error message */
      dcl errlong	      char (100) aligned;
				/* expanded error line for message */
      dcl errlong_len     fixed bin (35);
				/* length of errlong */
      dcl error_char      (const.max_seg_chars - 400) unal char (1)
		      based (addr (error.text));
      dcl errshort	      char (8) aligned;
				/* short error string for
				   convert_status_code_ */
      dcl icall	      fixed bin;	/* working index */
      dcl info_ptr	      ptr;	/* pointer to info structure */
      dcl line	      char (1020) var;
				/* offending line */
      dcl me	      char (32) var;/* entry used */
      dcl message	      char (512) var;
				/* error message */
      dcl next_err	      char (10000) based (next_err_ptr);
				/* string overlay for
				   next error message */
      dcl next_err_ptr    ptr;
      dcl NL	      char (1) static options (constant) init ("
");
      dcl sev	      fixed bin;	/* error severity */

      dcl (addr, max, null, size)
		      builtin;

      dcl comp_abort      condition;

      dcl com_err_	      entry options (variable);
      dcl convert_status_code_
		      entry (fixed (35), char (8) aligned,
		      char (100) aligned);
      dcl get_temp_segment_
		      entry (char (*), ptr, fixed bin (35));
      dcl ioa_$rsnp	      entry options (variable);
      dcl ioa_$general_rs entry (ptr, fixed bin, fixed bin, char (*),
		      fixed bin (21), bit (1) aligned, bit (1) aligned);

      ctl_str = "0"b;		/* not control string entry */
      me = "comp_report_:";
      sev = a_sev;
      code = a_code;
      message = a_message;
      line = a_line;
      info_ptr = a_info_ptr;
      goto JOIN;

ctlstr:
   entry;
/**** (a_sev, a_code, a_info_ptr, a_line, ctl_str, args... */

      dcl arglist	      (0:nargs) ptr based (arglist_ptr);
				/* argument ptr list */
      dcl arglist_ptr     ptr;	/* pointer to argument list */
      dcl fb	      fixed bin (35) based;
      dcl 1 msg	      based (msg_ptr),
	  2 len	      fixed bin (21),
	  2 chars	      char (512);
      dcl msg_ptr	      ptr;
      dcl nargs	      fixed bin;	/* argument count */
      dcl ptr	      ptr based;	/* to hold info ptr */

      dcl cu_$arg_count   entry (fixed bin);
      dcl cu_$arg_list_ptr
		      entry (ptr);

      ctl_str = "1"b;		/* control string entry */
      arglist_ptr = null;
      msg_ptr = addr (message);
      me = "comp_report_$ctlstr:";
      call cu_$arg_count (nargs);	/* number of args */
      call cu_$arg_list_ptr (arglist_ptr);
				/* pointer to arg list */
      sev = arglist (1) -> fb;
      code = arglist (2) -> fb;
      info_ptr = arglist (3) -> ptr;
      line = addrel (arglist (4), -1) -> txtstr;

      call ioa_$general_rs (arglist_ptr, 5, 6, msg.chars, msg.len, "0"b, "0"b);

JOIN:
      if shared.pass_counter > 1	/* if not the output pass */
      then return;			/* forget it */

      if shared.bug_mode
      then call ioa_ ("^a ^o ^a", me, code, message);

      if const.errblk_ptr = null ()
      then call error_init;

      if code ^= 0			/* convert code if given */
      then
         do;
	  call convert_status_code_ (code, errshort, errlong);
	  errlong_len = 101 - verify (reverse (errlong), " ");
         end;

      else
         do;
	  errlong = "";
	  errlong_len = 0;
         end;

      compose_severity_ = max (compose_severity_, sev);

      call build_call_nest;		/* build the call nest */

      if option.output_file_opt | option.check_opt
				/* direct reporting */
      then
         do;
	  if error.next = 0
	  then call ioa_ ("^/compose error list: (Vers. ^a)",
		  const.comp_version);
	  call ioa_ ("^a", call_nest);
	  call ioa_ ("^5x^[^a ^;^s^]^a^[^/^5x^a^]", (errlong_len > 0),
	       substr (errlong, 1, errlong_len), message, (line ^= ""),
	       comp_util_$display (translate (line, " ", ""), 0, "0"b));
	  error.next = 1;
         end;

      else
         do;			/* deferred reporting */
	  next_err_ptr = addr (error_char (error.next + 1));
	  call ioa_$rsnp ("^a", next_err, errlen, call_nest);
	  error.next = error.next + errlen;
	  next_err_ptr = addr (error_char (error.next + 1));
	  call ioa_$rsnp ("^5x^[^a ^;^s^]^a^[^/^5x^a^]", next_err, errlen,
	       (errlong_len > 0), substr (errlong, 1, errlong_len), message,
	       (line ^= ""),
	       comp_util_$display (translate (line, " ", ""), 0, "0"b));
	  error.next = error.next + errlen;
	  error.count = error.count + 1;
         end;

      return;

exact:
   entry (a_message, a_info_ptr);

      message = a_message;
      info_ptr = a_info_ptr;

      if shared.bug_mode
      then call ioa_ ("comp_report_$exact: ^a", message);

      if shared.pass_counter > 1	/* if not the output pass */
      then return;			/* forget it */

      if const.errblk_ptr = null ()
      then call error_init;

      call build_call_nest;		/* build the call nest */

      if option.output_file_opt | option.check_opt
				/* direct reporting to the user */
      then
         do;
	  if error.next = 0
	  then call ioa_ ("^/compose error list: (Vers. ^a)",
		  const.comp_version);
	  call ioa_ ("^a", call_nest);
	  call ioa_ ("^5x^a", a_message);
	  error.next = 1;
         end;

      else
         do;			/* deferred reporting */
	  next_err_ptr = addr (error_char (error.next + 1));
	  call ioa_$rsnp ("^a", next_err, errlen, call_nest);
	  error.next = error.next + errlen;
	  next_err_ptr = addr (error_char (error.next + 1));
	  call ioa_$rsnp ("^5x^a", next_err, errlen, a_message);
	  error.next = error.next + errlen;
	  error.count = error.count + 1;
         end;

error_init:
   proc;
      call get_temp_segment_ ("compose", const.errblk_ptr, ercd);
      if ercd ^= 0
      then
         do;
	  call com_err_ (ercd, "compose",
	       "Getting a temp segment for the error list.");
	  signal comp_abort;
	  return;
         end;
      error.count, error.next = 0;	/* initialize list */
   end error_init;

build_call_nest:
   proc;

      dcl i	      fixed bin;	/* working index */
      dcl (hbound, min)   builtin;

      do i = 0 to min (call_stack.index, hbound (call_stack.ptr, 1));
         call_box_ptr = call_stack.ptr (i);
         call_nest = call_nest || call_box.refname;
         call_nest = call_nest || "; ";
         if i < call_stack.index
         then call_nest = call_nest || ltrim (char (call_box.exit_lineno));
         else call_nest = call_nest || ltrim (char (line_info.lineno));
         call_nest = call_nest || ": ";
      end;

   end build_call_nest;
%page;
%include comp_entries;
%include comp_error;
%include comp_fntstk;
%include comp_insert;
%include comp_option;
%include comp_shared;
%include comp_text;
%include compstat;

   end comp_report_;
