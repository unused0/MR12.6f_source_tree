/****^  ***********************************************************
        *                                                         *
        * Copyright, (C) Honeywell Bull Inc., 1988                *
        *                                                         *
        * Copyright, (C) Honeywell Information Systems Inc., 1981 *
        *                                                         *
        * Copyright (c) 1972 by Massachusetts Institute of        *
        * Technology and Honeywell Information Systems, Inc.      *
        *                                                         *
        *********************************************************** */




/****^  HISTORY COMMENTS:
  1) change(86-01-21,LJAdams), approve(86-01-21,MCR7327),
     audit(86-04-17,Lippard), install(86-04-24,MR12.0-1048):
     Added ssu_ references so subsytem call to help_ work properly.  Added
     include file "help_args" which contains the new version number for help.
  2) change(88-08-03,RWaters), approve(88-08-03,MCR7950),
     audit(88-09-29,Huen), install(88-10-07,MR12.2-1146):
     Bug fixes for MR12.2.
                                                   END HISTORY COMMENTS */


/**** format: ind3,ll80,initcol6,indattr,^inddcls,dclind4,idind16	       */
/**** format: struclvlind2,^ifthenstmt,^ifthendo,^ifthen,^indnoniterdo       */
/**** format: ^inditerdo,^indnoniterend,^indthenelse,case,^indproc,^indend   */
/**** format: ^delnl,^insnl,comcol41,^indcom,^indblkcom,linecom,^indcomtxt   */

tedhelp_: proc (rstr);

dcl rstr		char (*);

/* UPDATE HISTORY						       */
/* EL#   date	TR	comments				       */
/* 143 84-10-10 phx17314 "help <info> -about <topic>" == "help <info>"       */
/* modified April, 1985 by L. Adams - use new help_args_ incl file           */
/* 202 88-07-08 phx20819 sci_ptr must be set to null()                       */

%include help_args_;
%include tedcommon_;
dcl 1 buf_des, 2 des;		/* These 2 lines are to fulfill      */
dcl 1 seg_des, 2 des;		/* ..refs in tedcommon.	       */
%page;
dcl about_sw	bit (1);
dcl err_ct	fixed bin;
dcl error_table_$badopt fixed bin (35) ext static;
dcl error_table_$nomatch fixed bin (35) ext static;
dcl first_rule_p	ptr;
dcl i		fixed bin;
dcl me		char (8) int static init ("ted_help");
dcl msg		char (168) var;
dcl code		fixed bin (35);
dcl msg_sw	bit (1);
dcl bar_info	bit (1);
dcl progress	fixed bin;
				/* =1: bad help_args version	       */
				/* =2: no pathnames given.	       */
				/* =3: evaluating pathnames.	       */
				/* =4: finding help segs.	       */
				/* =5: -section/-search	       */
				/*     & printing help segs.	       */
dcl rstr_b	fixed bin;
dcl sci_ptr         ptr;
dcl sec_sw	bit (1);
dcl state		fixed bin;
dcl tp		ptr;
dcl dname		char (168);
dcl command_error	condition;

dcl com_err_	entry options (variable);
dcl convert_date_to_binary_ entry (char (*), fixed bin (71), fixed bin (35));
dcl hcs_$fs_get_path_name entry (ptr, char (*), fixed bin, char (*),
		fixed bin (35));
dcl hcs_$make_ptr	entry (ptr, char (*), char (*), ptr, fixed bin (35));
dcl hcs_$status_minf entry (char(*), char(*), fixed bin(1), fixed bin(2),
		fixed bin(24), fixed bin(35));
dcl ioa_		entry options (variable);

dcl ssu_$destroy_invocation    	entry (ptr),
    ssu_$standalone_invocation	entry (ptr, char(*), char(*), ptr, entry, fixed bin(35));


dcl (addr, codeptr, index, length, null, rtrim, string, substr, verify
    ) builtin;

dcl cleanup                              condition;


      call help_$init (me, "info", "", Vhelp_args_3, Phelp_args, code);
      if (code ^= 0)
      then call com_err_ (code, me, "init");

      help_args.Sctl.title = "1"b;
      help_args.Lspace_between_infos = 1;
      bar_info = "0"b;
      help_args.min_Lpgh = 2;
      help_args.Npaths = 1;
      help_args.path (1).value = "ted";
      string (help_args.path (1).S) = "0"b;
      help_args.dir (1, 1) = "";
      help_args.ent (1) = "";
      help_args.S (1).info_name_not_starname = "1"b;
/* RW 88 */
      help_args.sci_ptr = null;                                       /*#202*/

xxxxx: first_rule_p = codeptr (xxxxx);	/* get pointer to me	       */
      rstr_b = verify (rstr, " ");	/* skip any leading SP	       */
      msg_sw, about_sw, sec_sw = "0"b;
      state = 1;

/* .   1 	     2       3       4       5       6       7 <-- STATE	       */
/* .   |func   info    section -about  topic   -from X		       */
/* . Expected combinations:					       */
/* .           info						       */
/* .           info    section				       */
/* .           **      section				       */
/* .                           -about  topic			       */
/* .				       -from X		       */
/* .           **      section                 -from X		       */
/* .   |func						       */
/* .   |func   info						       */
/* .   |info                   -about  topic			       */
/* .   -msg    xxx)etc					       */

      do while (rstr_b < length (rstr));
         i = index (substr (rstr, rstr_b), " ");
         if (i = 0)
         then i = length (rstr) - rstr_b;
         else i = i - 1;
         if (i > 1)
         then do;
	  if (substr (rstr, rstr_b, 1) = "|")
	  then do;
	     if (state ^= 1)
	     then do;
	        msg = "External function name must be first.";
	        goto err_ret;
	     end;
	     call find_external_info;
	     bar_info = "1"b;
	     state = 2;
	     goto update;
	  end;
	  if (substr (rstr, rstr_b, i) = "-msg")
	  then do;
	     if (state = 1)
	     then do;
	        help_args.title = "0"b;
	        msg_sw = "1"b;
	        help_args.path (1).value = "ted_msgs";
	        state = 2;
	        goto update;
	     end;
	  end;
	  if (substr (rstr, rstr_b, i) = "-about")
	  then do;
	     help_args.title = "0"b;				/* #143*/
	     if (state < 3)
	     then do;
	        help_args.info_name (1) = "**";
	        help_args.S (1).info_name_not_starname = "0"b;
	        about_sw = "1"b;
	        state = 5;
	        goto update;
	     end;
	     if (state < 5)
	     then do;
	        state = 5;
	        goto update;
	     end;
	     msg = "Misplaced -about.";
	     goto err_ret;
	  end;
	  if (substr (rstr, rstr_b, i) = "-from")
	  then do;
	     if (state = 5)
	     then do;
	        msg = "-from cannot follow -about.";
	        goto err_ret;
	     end;
	     if (state = 1)
	     then do;
	        help_args.Sctl.he_only = "1"b;
	        help_args.Sctl.he_info_name = "1"b;
	        help_args.Sctl.he_counts = "1"b;
	        help_args.info_name (1) = "**";
	        help_args.S (1).info_name_not_starname = "0"b;
	     end;
	     rstr_b = rstr_b + i;
	     i = length (rstr) - rstr_b;
	     msg = substr (rstr, rstr_b, i);
	     call convert_date_to_binary_ ((msg),
	        help_args.min_date_time, code);
	     if (code ^= 0)
	     then goto err_ret;
	     goto update;
	  end;
	  if (substr (rstr, rstr_b, 1) = "-")
	  then do;
	     msg = substr (rstr, rstr_b, i);
	     code = error_table_$badopt;
	     goto err_ret;
	  end;
         end;
         if (state < 3)
         then do;
	  help_args.info_name (1) = substr (rstr, rstr_b, i);
	  if (help_args.info_name (1) = "**")
	  then help_args.info_name_not_starname (1) = "0"b;
	  state = 3;
	  if msg_sw
	  then if (i > 5)
	       then if (substr (help_args.info_name (1), 5, 1) = "|")
		  then do;	/* external function error	       */
		     rstr_b = rstr_b + 4;
		     i = i - 4;
		     call find_external_info;

/* The form here is						       */
/*      -msg xxx)|yyyy					       */
/* The action to be done is to search for ted_yyyy_ and then use the	       */
/*  directory containing it as the search directory. The segment looked for  */
/*  is ted_yyyy_.info.  The info looked for is "xxx)|yyyy". If that is not   */
/*  found, then "xxx)" is looked for.				       */

		  end;
	  goto update;
         end;
         if (state = 3)
         then do;
	  help_args.title = "0"b;
	  help_args.Sctl.scn, sec_sw = "1"b;
	  help_args.Nscns = 1;
	  help_args.scn (1) = substr (rstr, rstr_b, i);
	  state = 4;
	  goto update;
         end;
         if (state = 5)
         then do;
	  help_args.Nsrhs = 1;
	  help_args.Sctl.srh = "1"b;
	  i = length (rstr) - rstr_b;
	  help_args.srh = substr (rstr, rstr_b, i);
	  goto update;
         end;
         msg = "Improper arguments.";
err_ret:
         call com_err_ (code, me, "^a", msg);
         goto return_;
update:
         rstr_b = rstr_b + i;
         rstr_b = rstr_b - 1 + verify (substr (rstr, rstr_b), " ");
				/* skip any leading SP	       */
      end;


      on condition (command_error) begin;
dcl 1 command_error_info aligned based (cond_info.infoptr),
      2 length	fixed bin,
      2 version	fixed bin init (2),
      2 action_flags,
        3 cant_restart bit (1) unal,
        3 default_restart bit (1) unal,
        3 reserved	bit (34) unal,
      2 info_string char (256) var,
      2 status_code fixed bin (35),
      2 name_p	ptr,
      2 name_l	fixed bin,
      2 msg_p	ptr,
      2 msg_l	fixed bin,
      2 msg_maxl	fixed bin,
      2 print_sw	bit (1);
dcl 1 cond_info	aligned,
%include cond_info;
dcl find_condition_info_ entry (ptr, ptr, fixed bin (35));

	  call find_condition_info_ (null (), addr (cond_info), code);
	  if (code = 0)
	  then do;
	     command_error_info.print_sw = "0"b;
	     err_ct = err_ct + 1;
	  end;

         end;
dcl l fixed bin;
      call hcs_$fs_get_path_name (first_rule_p, dname, l, "", code);
      if (code ^= 0)
      then do;
         call com_err_ (code, me, "Getting pathname from ^p", first_rule_p);
         goto return_;
      end;
      call hcs_$status_minf (dname, help_args.path (1).value || ".info",
         0, 0, 0, code);
      if (code = 0)			/* if name was found, use that'un    */
      then help_args.path (1).value
         = rtrim (dname) || ">" || help_args.path (1).value;

re_help:
      err_ct = 0;
      sci_ptr = null;

      on cleanup
         begin;
         if Phelp_args ^= null then
	  call ssu_$destroy_invocation (help_args.sci_ptr);
         else if sci_ptr ^= null then
	  call ssu_$destroy_invocation (sci_ptr);
         end;
      
      call ssu_$standalone_invocation (sci_ptr, me, (ted_vers), null, abort_help_command, code);
      if code ^= 0 then
         call com_err_ (code, me, "Unable to invoke ssu.");

      help_args.sci_ptr = sci_ptr;

      call help_ (me, Phelp_args, "info", progress, code);
      if (err_ct > 0) & msg_sw
      then do;
         if (substr (help_args.info_name (1), 4, 1) = ")")
         then do;
	  substr (help_args.info_name (1), 4) = "";
	  goto re_help;
         end;
         call ioa_ ("No additional help available.^/");
         code = 0;
      end;
      if (code ^= 0)
      then do;
         if (progress = 3)
         then code = help_args.path (1).code;
         if (progress = 5) & (sec_sw | about_sw) & (err_ct = 0)
         then call ioa_ (
	       "^[^; Info ""^a"" does not contain section ""^a"""
	       || "^[ (in ^a)^]^/^]", about_sw, help_args.info_name (1),
	       help_args.scn (1), bar_info, help_args.search_dirs (1));
         else if (progress = 4)
         then call ioa_ ("Info segment not found. ^a.info",
	       help_args.value (1));
         else do;
	  if (code = error_table_$nomatch)
	  then call ioa_ ("No info found. ^a^[ (in ^a)^]",
		help_args.info_name (1), bar_info, help_args.search_dirs (1));
	  else call com_err_ (code, me);
         end;
      end;

return_:
      if Phelp_args ^= null then
         call ssu_$destroy_invocation (help_args.sci_ptr);
      else if sci_ptr ^= null then
         call ssu_$destroy_invocation (sci_ptr);
      call help_$term (me, Phelp_args, 0);
      return;


abort_help_command:
      proc;
      
      return;
end abort_help_command;


find_external_info: proc;
      help_args.value (1) = "ted_";
      help_args.info_name (1) = "";
      help_args.value (1)
         = help_args.value (1) || substr (rstr, rstr_b + 1, i - 1);
      help_args.value (1) = help_args.value (1) || "_";

      call hcs_$make_ptr (first_rule_p, (help_args.value (1)),
         (help_args.value (1)), tp, code);
      if (code ^= 0)
      then do;
         call com_err_ (code, me, "Searching for ^a",
	  help_args.value (1));
         goto return_;
      end;
      first_rule_p = tp;
   end;

   end tedhelp_;
