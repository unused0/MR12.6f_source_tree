/* ***********************************************************
   *                                                         *
   *                                                         *
   * Copyright, (C) Honeywell Information Systems Inc., 1981 *
   * Copyright, (C) Honeywell Information Systems Inc., 1980 *
   *                                                         *
   *                                                         *
   *********************************************************** */

&ext device=ascii&;

&ext notes=
/* This is a 10 pitch ascii typewriter. It has no plotting capability. There */
/* is an attempt to represent artwork constructs in an understandable	       */
/* (if not always pretty) fashion.				       */
&;
&ext devclass=printer&;

&ext dcls=
&.    dcl 1 bead	   (8192) aligned,	/* bead structure for canonizing */
	2 loc	   fixed bin,	/* column position */
	2 char	   char (1);	/* the character */
    dcl beadct	   fixed bin;	/* count of beads */
				/* bead array for debug */
    dcl 1 beads	   (beadct) aligned based (beadp),
	2 loc	   fixed bin,
	2 char	   char (1);
    dcl beadp	   ptr;
    dcl BSCR	   char (2) static options (constant) init ("");
    dcl d		   fixed bin;	/* bead separation for sorting */
    dcl icol	   fixed bin;	/* working column position */
    dcl ii	   fixed bin;	/* working index */
    dcl MAX_STR	   fixed bin static options (constant) init (1024);
    dcl ocol	   fixed bin;	/* working column position */
    dcl PENDOWN	   char (1) init ("_") static options (constant);
    dcl PENUP	   char (1) init (" ") static options (constant);
    dcl scndx	   fixed bin (21);	/* output scanning index */
    dcl space	   fixed bin;	/* bead separation space */
    dcl swps	   fixed bin;	/* # of swaps in a sort pass */
				/* temp for sorting beads */
    dcl tbead	   bit (72) aligned;

    dcl (char, copy, search, rank)
		   builtin;
&;

&ext page_init=
     beadp = addr (bead);
&;

&ext file_init=
&.   max_revlead = 0;		/* ascii cant back up */&+
&;

&ext put=&+
    if tstr.open			/* is this line open? */
      then
        do;			/* see if canonizing is needed */
	if search (tstr_line, BSCR) > 0/* any overprinting? */
	then
	  do;
	    scndx = 1;
	    beadct, icol = 0;	/* clear counters */
				/* scan the input line */
	    do j = 1 to length (tstr_line);
				/* extract next char */
	      tchr = substr (tstr_line, j, 1);
                                        /* all printing chars are 1 */
	      if (rank (tchr) > 32 && rank (tchr) <= 126)
	      then
	        do;
	          beadct = beadct + 1;/* count a bead */
	          bead (beadct).char = tchr;
				/* note (apparent) position */
	          bead (beadct).loc = icol;
	          icol = icol + 1;	/* and advance */
	        end;

	      else if tchr = " "	/* a space? */
	      then
	        do;		/* how many? */
	          i = verify (substr (tstr_line, j), " ") - 1;
	          if i < 0		/* trailing ws */
	          then i = length (tstr_line) - j + 1;
	          icol = icol + i;
	          j = j + i - 1;	/* advance scan index */
	        end;

	      else if tchr = BSP	/* a backspace? */
	      then
	        do;		/* how many? */
	          i = verify (substr (tstr_line, j), BSP) - 1;
	          if i < 0		/* trailing BSPs */
	          then i = length (tstr_line) - j + 1;
				/* dont back off end */
	          icol = max (icol - i, 0);
	          j = j + i - 1;	/* advance scan index */
	        end;

	      else if tchr = CR	/* a carriage return? */
	      then icol = 0;		/* go back to square 1! */

	      else if tchr = HT	/* may be HTs from SHIFTs */
	      then icol = 10 * divide (icol, 10, 17, 0) + 10;
	    end;			/* end of scan loop */
				/* sort the beads */
	    if long_sw
              then call
		 ioa_ ("^5x(beads=(^d) ^v(^a^))", beadct, beadct,
		 beads.char);

	    d = beadct;
sort:
	    d = divide (d + 1, 2, 17, 0);
	    swps = 0;
              do i = 1 to beadct - d;	/* sort columns only */
(nosubrg):      if bead (i).loc > bead (i + d).loc
                then goto swap;
	      else		/* make _'s first */
(nosubrg):      if bead (i).loc = bead (i + d).loc
                then if bead (i + d).char = "_" && bead (i).char ^= "_"
                     then
		   do;
swap:
		     tbead = unspec (bead (i));
                         bead (i) = bead (i + d);
                         unspec (bead (i + d)) = tbead;
                         swps = swps + 1;
                       end;
              end;

	    if long_sw
              then call
		 ioa_ ("^5x(swaps=^d@^d, beads ^v(^a^))", swps, d, beadct,
		 beads.char);

	    if swps > 0 | d > 1
              then goto sort;

	    ocol = 0;		/* set up for bead stringing */
	    tstr_line = "";
	    do j = 1 to beadct;	/* put sorted beads back into line */
				/* bead separation */
	      space = bead (j).loc - ocol;

	      if space > 0		/* any needed? */
	      then tstr_line = tstr_line || copy (" ", space);

	      if space < 0		/* overstrike? */
	      then tstr_line = tstr_line || BSP;
				/* finally, the character */
	      tstr_line = tstr_line || bead (j).char;
	      ocol = bead (j).loc + 1; /* next column */
	    end;			/* end of bead stringing loop */

	    if long_sw
	    then call
		 ioa_ ("^7x(canon: ^d ^f ""^a"")", length (tstr_line),
	           show (Xpos, 12000), comp_util_$display (tstr_line, 0, "0"b));
           end;			/* end of overprint loop */
				/* trim trailing WS */
	 tstr_line = rtrim (tstr_line);
           tstr.last_cr = page_record.leng;
         end;

    tstr_line = tstr_line || NL;
&;&+

&ext plot=&+
	if ^PLOT_OP
	then
	  do;
	    if xmove ^= 0		/* any X movement? */
	    then
	      do;
	        if xmove > 0
	        then copystr = copy (" ", xmove);
	        else copystr = copy (BSP, -xmove);
	        pltstr = pltstr || copystr;
	        pltwidth = pltwidth + xmove;
	        xmove = 0;
	      end;
	  end;

	else
	 do;
	   if ymove ^= 0		/* no vertical vectors allowed       */
	   then call comp_report_$exact ("Vertical vectors not allowed " ||
		"for the printer device.", lineinfoptr);

	   if xmove < 0		/* no rev horiz vectors allowed      */
	   then call comp_report_$exact ("Reverse horizontal vectors not " ||
		"allowed for ascii device.", lineinfoptr);

	   else if xmove > 0	/* forward horizontal vector */
	   then
	     do;
	       pltstr = pltstr || copy ("_", xmove);
	       pltwidth = pltwidth + xmove;
	       xmove = 0;
	     end;
	  end;
&;&+

&ext set_media=
       medselstr = "";
&;&+

&ext set_ps=
       media_size = 7200;
       font_media (font_in) = 1;	/* ascii has only one */
&;&+

&ext foot_proc=
      footref (1) = "(";
      footref (3) = ")";
&;

&comp_dev_writer()
