/****^  ***********************************************************
        *                                                         *
        * Copyright, (C) BULL HN Information Systems Inc., 1989   *
        *                                                         *
        * Copyright, (C) Honeywell Information Systems Inc., 1982 *
        *                                                         *
        * Copyright (c) 1972 by Massachusetts Institute of        *
        * Technology and Honeywell Information Systems, Inc.      *
        *                                                         *
        *********************************************************** */




/****^  HISTORY COMMENTS:
  1) change(89-04-23,Zimmerman), approve(89-04-23,MCR8060),
     audit(89-05-05,RWaters), install(89-05-24,MR12.3-1048):
     MCR8060 cobol_divide_bin_gen.pl1 Reformatted code to new Cobol standard.
                                                   END HISTORY COMMENTS */


/* Modified on 06/29/79 by FCH, [4.0-1], not option added for debug */
/* Modified since Version 4.0 */

/*{*/
/* format: style3 */
cobol_divide_bin_gen:
     proc (in_token_ptr, next_stmt_tag);

/*
This procedure generates code for the divide statement
which uses the hardware registers ( A and Q ) instead
of using EIS instructions.  */

/*  DECLARATION OF THE PARAMETERS  */

/* dcl in_token_ptr ptr;  */
/*  Declared below in an include file.  */
dcl	next_stmt_tag	fixed bin;

/*  DESCRIPTION OF THE PARAMETERS  */

/*
PARAMETER		DESCRIPTION

in_token_ptr	Pointer to a structurre that contains data
		and pointers that describes the divide
		statement for which code is to be
		implemented.  (input)  See description
		below for more details.
next_stmt_tag	A tag that is to be defined at the next
		cobol statement by cobol_gen_driver_.
		(output)  See below for details.
*/


/*  DECLARATION OF EXTERNAL ENTRIES  */

dcl	cobol_make_bin_const
			ext entry (ptr, ptr, fixed bin);
dcl	cobol_short_to_longbin$register
			ext entry (ptr, ptr);
dcl	cobol_load_register ext entry (ptr, ptr);
dcl	cobol_short_to_longbin$temp
			ext entry (ptr, ptr);
dcl	cobol_alloc$stack	ext entry (fixed bin, fixed bin, fixed bin (24));
dcl	cobol_make_type9$long_bin
			ext entry (ptr, fixed bin, fixed bin (24));
dcl	cobol_store_binary	ext entry (ptr, ptr, bit (1));
dcl	cobol_register$release
			ext entry (ptr);
dcl	cobol_addr	ext entry (ptr, ptr, ptr);
dcl	cobol_emit	ext entry (ptr, ptr, fixed bin);
dcl	cobol_fofl_mask$on	ext entry;
dcl	cobol_fofl_mask$off ext entry;
dcl	cobol_multiply2_binary
			ext entry (ptr, ptr, ptr, fixed bin);
dcl	cobol_make_tagref	ext entry (fixed bin, fixed bin, ptr);
dcl	cobol_define_tag	ext entry (fixed bin);
dcl	cobol_register$load ext entry (ptr);
dcl	cobol_make_reg_token
			ext entry (ptr, bit (4));

/*  DECLARATION OF INTERNAL STATIC DATA  */

dcl	STZ		bit (10) int static init ("1001010000"b);
						/*  450(0)  */
dcl	AOS		bit (10) int static init ("0001011000"b);
						/*  054(0)  */
dcl	LDA		bit (10) int static init ("0100111010"b);
						/*  235(0)  */
dcl	LDQ		bit (10) int static init ("0100111100"b);
						/*  236	(0)  */

dcl	tov_inst		bit (36) int static init ("000000000000000000110001111000000000"b);
	;					/*  tov 0  */

dcl	tra_inst		bit (36) int static init ("000000000000000000111001000000000000"b);
						/*  tra 0  */

dcl	tnz_inst		bit (36) int static init ("000000000000000000110000001000000000"b);
						/*  tnz 0  */


dcl	tze_inst		bit (36) int static init ("000000000000000000110000000000000000"b);
						/*  tze 0  */
dcl	1 dec_zero_token	int static,
	  2 size		fixed bin (15),
	  2 line		fixed bin (15),
	  2 column	fixed bin (15),
	  2 type		fixed bin (15) init (2),
	  2 integral	bit (1) init ("1"b),
	  2 floating	bit (1) bit (1) init ("0"b),
	  2 filler1	bit (5),
	  2 subscript	bit (1) init ("0"b),
	  2 sign		char (1) init (" "),
	  2 exp_sign	char (1) init (" "),
	  2 exp_places	fixed bin (15),
	  2 places_left	fixed bin (15) init (1),
	  2 places_right	fixed bin (15) init (0),
	  2 places	fixed bin (15) init (1),
	  2 literal	char (1) init ("0");

/*  DECLARATION OF INTERNAL VARIABLES  */

dcl	1 input_buff,
	  2 buff		(1:10) fixed bin;

dcl	1 reloc_buff,
	  2 buff		(1:10) bit (5) aligned;



dcl	1 register_struc,
	  2 what_reg	fixed bin,
	  2 reg_no	bit (4),
	  2 lock		fixed bin,
	  2 already_there	fixed bin,
	  2 contains	fixed bin,
	  2 tok_ptr	ptr,
	  2 literal	bit (36);
dcl	result_token_ptr	ptr;
dcl	work_token_ptr	ptr;
dcl	addend_token_ptr	ptr;
dcl	receive_count	fixed bin;
dcl	ret_offset	fixed bin (24);
dcl	ovflo_flag_inst	bit (36);
dcl	ovflo_tag		fixed bin;
dcl	no_ovflo_tag	fixed bin;
dcl	imperative_stmt_tag fixed bin;
dcl	ix		fixed bin;
dcl	temp_target_code	fixed bin;
dcl	divisor_token_ptr	ptr;
dcl	dividend_token_ptr	ptr;
dcl	remainder_token_ptr ptr;
dcl	ose_flag		bit (1);
dcl	tlength		fixed bin;
dcl	temp_ptr		ptr;
dcl	skipped_some	bit (1);
dcl	temp_lop_token_ptr	ptr;
dcl	temp_rop_token_ptr	ptr;


dcl	call_again	bit (1);


dcl	dn_ptr		ptr;

dcl	last_target_index	fixed bin;

/*************************************/
start:						/*  Extract useful information from the EOS token.  */
	eos_ptr = in_token.token_ptr (in_token.n);
	ose_flag = end_stmt.b;

	if ose_flag
	then do;					/*  Reserve two tags for on size error processing.  */
		imperative_stmt_tag = cobol_$next_tag;
		next_stmt_tag = imperative_stmt_tag + 1;
		cobol_$next_tag = cobol_$next_tag + 2;
	     end;					/*  Reserve two tags for on size error processing.  */
	result_token_ptr = null ();

	if end_stmt.a = "000"b
	then call format1_divide;
	else call format2_5_divide;


/*************************************/
format1_divide:
     proc;

/*
This internal procedure generates code using the hardware
registers ( A and Q ) for format 1 divide statements.  */

	receive_count = end_stmt.e;
	divisor_token_ptr = in_token.token_ptr (2);

/*  Check for zero divisor if on size clause was present in the divide stmt.  */
	if ose_flag
	then call zero_divide_check (divisor_token_ptr, imperative_stmt_tag);
	if (divisor_token_ptr -> data_name.type = rtc_dataname & divisor_token_ptr -> data_name.bin_18)
	then do;					/*  Divisor is short binary.  */
						/*  Convert from short binary to long binary into a temp.  */
		temp_ptr = null ();
		call cobol_short_to_longbin$temp (divisor_token_ptr, temp_ptr);
		divisor_token_ptr = temp_ptr;
	     end;					/*  Divisior is a short binary.  */

	else if (divisor_token_ptr -> data_name.type = rtc_dataname & receive_count > 1)
	then do;					/*  Divisor is long binary, and more than one dividend/receiving field.  */
						/*  Generate code to store the divisor into a temp, because if one of
		the dividends is the divisor (i.e. DIVIDE A INTO A B C) then the original
		divisor value will be destroyed.  */

/*  Allocate space for the temporary in the stack.  */
		call cobol_alloc$stack (4, 0, ret_offset);
		temp_ptr = null;			/*  Make a data name token for the temp.  */
		call cobol_make_type9$long_bin (temp_ptr, 1000, ret_offset);
						/*  Store the divisor into the temporary.  */
		call cobol_store_binary (divisor_token_ptr, temp_ptr, call_again);
						/*  Release the register that was used in storing the divisor.  */
		register_struc.reg_no = divisor_token_ptr -> cobol_type100.register;
		call cobol_register$release (addr (register_struc));
		divisor_token_ptr = temp_ptr;
	     end;					/*  Divisor is long binary, and move than one dividend/receiving field.  */



/*  Generate code to divide the divisor into each dividend/receiving field.  */
	do ix = 3 to in_token.n - 1;			/*  Do all the divides.  */

	     call cobol_multiply2_binary (in_token.token_ptr (ix), divisor_token_ptr, result_token_ptr, 2);
						/*  Make a register token that describes the result of the divide.  */
	     result_token_ptr = null ();
	     call cobol_make_reg_token (result_token_ptr, "0010"b /* Q */);


	     call cobol_store_binary (result_token_ptr, in_token.token_ptr (ix), call_again);
	     if call_again
	     then do;				/*  Must call the store procedure again to get the results stored.  */


		     call cobol_store_binary (result_token_ptr, in_token.token_ptr (ix), call_again);
		end;				/*  Must call the store procedure again to get the results stored.  */
						/*  Release the register that contains the result of the divide.  */
	     register_struc.reg_no = result_token_ptr -> cobol_type100.register;
	     call cobol_register$release (addr (register_struc));
						/*  Release the A register, which is locked by the multiply2 procedure.  */
	     register_struc.reg_no = "0001"b;		/*  A  */
	     call cobol_register$release (addr (register_struc));


	end;					/*  Do all the divides.  */


	if ose_flag
	then do;					/*  On size error clause was present.  */

/*[4.0-1]*/
		if end_stmt.f = "01"b		/*[4.0-1]*/
		then next_stmt_tag = imperative_stmt_tag;
						/*[4.0-1]*/
		else do;

/*  Generate code to transfer to the next cobol statement ( the one
			following the imperative statement. )  */
			call cobol_emit (addr (tra_inst), null (), 1);
			call cobol_make_tagref (next_stmt_tag, cobol_$text_wd_off - 1, null ());

/*  Define the imperative statement tag at the next instruction location.  */
			call cobol_define_tag (imperative_stmt_tag);

/*[4.0-1]*/
		     end;

	     end;					/*  On size error clause was present.  */
     end format1_divide;


/*************************************/
format2_5_divide:
     proc;

/*
This internal procedure generates code using the hardware
registers (A and Q) for format 2,3,4, and 5 divide statements.  */

	if (end_stmt.a = "001"b | end_stmt.a = "011"b)
	then do;					/*  Format 2 or Format 4 divide.  */
		divisor_token_ptr = in_token.token_ptr (2);
		dividend_token_ptr = in_token.token_ptr (3);
	     end;					/*  Format 2 or Format 4 divide.  */

	else do;					/*  Must be Format 3 or Format 5 divide.  */
		divisor_token_ptr = in_token.token_ptr (3);
		dividend_token_ptr = in_token.token_ptr (2);
	     end;					/*  Must be Format 3 or Format 5 divide.  */

/*  Check for zero divisor if on size clause was present.  */
	if ose_flag
	then call zero_divide_check (divisor_token_ptr, imperative_stmt_tag);

/*  Generate code to do the division.  */
	call cobol_multiply2_binary (dividend_token_ptr, divisor_token_ptr, result_token_ptr, 2 /*divide */);

	if (end_stmt.a = "001"b | end_stmt.a = "010"b)
	then do;					/*  Format 2 or Format 3 divide.  */
						/*  Release the A register (which is locked during the divide)  */
		register_struc.reg_no = "0001"b;	/* A */
		call cobol_register$release (addr (register_struc));
		last_target_index = in_token.n - 1;
	     end;					/*  Format 2 or Format 3 divide.  */

	else do;					/*  Format 4 or Format 5 divide.  */
						/*  Build a register token for the A register, which contains the remainder.  */
		remainder_token_ptr = null ();
		call cobol_make_reg_token (remainder_token_ptr, "0001"b /*A*/);

		last_target_index = in_token.n - 2;
	     end;					/*  Format 4 or Format 5 divide.  */

/*  Generate code to store the quotient into all long binary receiving fields.  */
/*  Note that there is no possibliity of overflow, since result is long binary, and so are targets.  */
	skipped_some = "0"b;

	do ix = 4 to last_target_index;		/*  Store quotient into all long binary targets.  */

	     if in_token.token_ptr (ix) -> data_name.bin_18
	     then skipped_some = "1"b;
	     else call cobol_store_binary (result_token_ptr, in_token.token_ptr (ix), call_again);
	end;					/*  Store quotient into all long binary targets.  */



	if skipped_some
	then do;					/*  Store the quotient into all short binary receiving fields.  */
		if ose_flag
		then call cobol_fofl_mask$on;

		do ix = 4 to last_target_index;	/*  Scan the targets.  */

		     if in_token.token_ptr (ix) -> data_name.bin_18
		     then do;			/*  Short binary target.  */
			     call cobol_store_binary (result_token_ptr, in_token.token_ptr (ix), call_again);
			     if call_again
			     then do;		/*  REsult has been moved to a temp in an attempt to force
				overflow.  */

				     if ose_flag
				     then do;	/*  On size error clause present.  */
						/*  Must test for overflow.  */
					     call cobol_emit (addr (tov_inst), null (), 1);
					     call cobol_make_tagref (imperative_stmt_tag,
						cobol_$text_wd_off - 1, null ());
					end;	/*  On size error clause present.  */

/*  Generate code to store the temp into the target.  */
				     call cobol_store_binary (result_token_ptr, in_token.token_ptr (ix),
					call_again);
				end;		/*  Result has been moved to a temp in an attempt to force
					overflow.  */

			end;			/*  Short binary target.  */
		end;				/*  Scan the targets.  */

	     end;					/*  Store the quotient into all short binary receiving fields.  */

	if (end_stmt.a = "011"b | end_stmt.a = "100"b)
	then do;					/*  Format 4 or Format 5 divide  */
						/*  Store the remainder (now in the a register) into the cobol target.  */
		if ose_flag & in_token.token_ptr (in_token.n - 1) -> data_name.bin_18
		then call cobol_fofl_mask$on;		/*  Turn on the fixed overflow mask.  */
		call cobol_store_binary (remainder_token_ptr, in_token.token_ptr (in_token.n - 1), call_again);

		if call_again
		then do;				/*  Remainder has been stored into a temp in an attempt to force overflow.  */
			if ose_flag
			then do;			/*  On size clause was present.  */
						/*  Test for overflow  */
				call cobol_emit (addr (tov_inst), null (), 1);
				call cobol_make_tagref (imperative_stmt_tag, cobol_$text_wd_off - 1, null ());
			     end;			/*  On size clause was present.  */

/*  Generate code to store the temp into the target.  */
			call cobol_store_binary (remainder_token_ptr, in_token.token_ptr (in_token.n - 1),
			     call_again);
		     end;				/*  Remainder has been stored inot a temp in an attempt to force overflow.  */

		if remainder_token_ptr -> data_name.type = rtc_register
		then do;				/*  Remainder token describes a register.  */
						/*  Release the register, since the value there has been stored . */
			register_struc.reg_no = remainder_token_ptr -> cobol_type100.register;
			call cobol_register$release (addr (register_struc));
		     end;				/*  Remainder token describes a register.  */

	     end;					/*  Format 4 or Format 5 divide.  */

	if ose_flag
	then do;					/*  On size error clause was present.  */
						/*  Generate code to turn off the fixed overflow mask bit.  */
		call cobol_fofl_mask$off;

/*[4.0-1]*/
		if end_stmt.f = "01"b		/*[4.0-1]*/
		then next_stmt_tag = imperative_stmt_tag;
						/*[4.0-1]*/
		else do;

/*  Emit code to transfer to the next cobol statement.  (The statement
		following the imperative statement.)  */
			call cobol_emit (addr (tra_inst), null (), 1);
			call cobol_make_tagref (next_stmt_tag, cobol_$text_wd_off - 1, null ());
						/*  Define the imperative statement tag at the next instruction location.  */
			call cobol_define_tag (imperative_stmt_tag);

/*[4.0-1]*/
		     end;

/*  Generate code to turn off the fixed overflow mask bit  */
		call cobol_fofl_mask$off;

	     end;					/*  On size error clause was present.  */

	if result_token_ptr -> data_name.type = rtc_register
	then do;					/*  Result token describes a register.  */
						/*  Release the register, since the value there has been stored into all receiving fields. */
		register_struc.reg_no = result_token_ptr -> cobol_type100.register;
		call cobol_register$release (addr (register_struc));
	     end;					/*  Result token describes a register.  */

     end format2_5_divide;


/*************************************/
zero_divide_check:
     proc (divisor_token_ptr, imperative_stmt_tag);

/*
This internal procedure generates code to
	a. test whether the divisor is zero
	b. and transfer immediately to the imperative statement if the divisor is zero.
*/

/*  DECLARATION OF THE PARAMETERS  */
dcl	divisor_token_ptr	ptr;
dcl	imperative_stmt_tag fixed bin;

/*  DECLARATION OF INTERNAL VARIABLES  */
dcl	work_token_ptr	ptr;

/*  Generate code to load the divisor into the A or Q  */
	work_token_ptr = null ();
	if divisor_token_ptr -> data_name.type = rtc_resword
	then divisor_token_ptr = addr (dec_zero_token);
	if divisor_token_ptr -> data_name.type = rtc_numlit
	then do;					/*  Divisor is a numeric literal token.  */
		call cobol_make_bin_const (divisor_token_ptr, work_token_ptr, 2);
		divisor_token_ptr = work_token_ptr;
		work_token_ptr = null ();
	     end;					/*  Divisor is a numeric literal token.  */
	if divisor_token_ptr -> data_name.type = rtc_dataname & divisor_token_ptr -> data_name.bin_18
						/*  divisor is short binary  */
	then call cobol_short_to_longbin$register (divisor_token_ptr, work_token_ptr);
	else call cobol_load_register (divisor_token_ptr, work_token_ptr);

/*  Emit a TZE instruction.  */
	call cobol_emit (addr (tze_inst), null (), 1);
	call cobol_make_tagref (imperative_stmt_tag, cobol_$text_wd_off - 1, null ());
						/*  Release the register which has been loaded with the divisor.  */
	register_struc.reg_no = work_token_ptr -> cobol_type100.register;
	call cobol_register$release (addr (register_struc));

     end zero_divide_check;

/*  INCLUDE FILES USED IN THIS PROCEDURE  */

/*****	Declaration for builtin function	*****/

dcl	(substr, mod, binary, fixed, addr, addrel, rel, length, string, unspec, null, index)
			builtin;

/*****	End of declaration for builtin function	*****/

%include cobol_type9;
%include cobol_addr_tokens;
%include cobol_;
%include cobol_in_token;
%include cobol_record_types;
%include cobol_type100;
%include cobol_type19;

     end cobol_divide_bin_gen;
