/****^  ***********************************************************
        *                                                         *
        * Copyright, (C) BULL HN Information Systems Inc., 1989   *
        *                                                         *
        * Copyright, (C) Honeywell Information Systems Inc., 1982 *
        *                                                         *
        *********************************************************** */


/****^  HISTORY COMMENTS:
  1) change(89-04-23,Zimmerman), approve(89-04-23,MCR8060),
     audit(89-05-05,RWaters), install(89-05-24,MR12.3-1048):
     MCR8060 cobol_add3.pl1 Added Trace statements.
                                                   END HISTORY COMMENTS */


/* format: style3 */
%;
/* ******************************************************
   *                                                    *
   *                                                    *
   * Copyright (c) 1972 by Massachusetts Institute of   *
   * Technology and Honeywell Information Systems, Inc. *
   *                                                    *
   *                                                    *
   ****************************************************** */

/* Modified on 10/19/84 by FCH, [5.3-1], BUG563, new cobol_addr_tokens.incl.pl1 */
/* Modified on 09/22/83  by FCH, [5.2...], trace added */

cobol_add3:
     proc (operand1_ptr, operand2_ptr, result_ptr, opcode_code);

	/***..... if Trace_Bit then call cobol_gen_driver_$Tr_Beg(MY_NAME);/**/


/*
This procedure generates code for the following types of Cobol constructs:

	1. ADD A B GIVING C.
	2. SUBTRACT A FROM B GIVING C.

This procedure makes one important assumption about the 
input operands:  The operands to be added (or subtracted) are
both represented by data name (type 9) tokens.  That is, any
conversion of the operands from numeric literal or figurative
constant has already been done before this procedure is called.

*/

/* Note that if the "rounded" bit is on in the token pointed at 
by result_ptr, then the code generated will perform
addition/subtraction with rounding.  */

/*  DECLARATION OF THE PARAMETERS  */

dcl	operand1_ptr	ptr;
dcl	operand2_ptr	ptr;
dcl	result_ptr	ptr;
dcl	opcode_code	fixed bin (35);

/*
operand1_ptr	Points to the token for the addend or
		minuend, depending on whether code is to
		be generated for addition or subtraction,
		respectively. (input)
operand2_ptr	Points to the token for the augend or
		subtrahend, depending on whether code is to be
		generated for addition or subtraction,
		respectively.  (input)
result_ptr	Points to the token to receive the sum
		or diffenence, depending on whether code it to
		be generated for addition or subtraction,
		respectively.  (input)
opcode_code	A code that indicates whether code is to be generated
		for an addition or subtraction.  (input)

			opcode_code	| meaning
			-------------------------------------
				1	| addition
				2	| subtraction

*/

/*  DECLARATION OF EXTERNAL ENTRIES  */

dcl	cobol_addr	ext entry (ptr, ptr, ptr);
dcl	cobol_emit	ext entry (ptr, ptr, fixed bin);


/*  DECLARATION OF INTERNAL STATIC VARIABLES  */

/*  Declaration of internal static variables that contain
	AdD3 and SUBTRACT3 opcodes  */

dcl	add3_op		bit (10) int static init ("0100100101"b /*222(1)*/);
dcl	subtract3_op	bit (10) int static init ("0100100111"b /*233(1)*/);


/*  DECLARATION OF INTERNAL AUTOMATIC VARIABLES  */

/*  Declaration of buffers used by the addressability utility  */

/*  Relocation info buffer  */
dcl	reloc_buffer	(1:10) fixed bin;

/*  instruction/descriptor buffer  */
dcl	addr_inst_buffer	(1:10) fixed bin;

/*  addressability input buffer  */
dcl	addr_input_buffer	(1:30) fixed bin;
dcl	dn_ptr		ptr;


/**************************************************/
/*	START OF EXECUTION			*/
/*	cobol_add3				*/
/**************************************************/


/*  Point pointers at the buffers used to establish addressability  */

	reloc_ptr = addr (reloc_buffer (1));
	input_ptr = addr (addr_input_buffer (1));
	inst_ptr = addr (addr_inst_buffer (1));

/*  Build the input structure to the addressability utility  */

	input_struc.type = 6;			/*  eis, 3 input operands, instruction word and 3 descriptors returned  */
	input_struc.operand_no = 3;
	input_struc.lock = 0;			/*  no locks  */

	input_struc.operand.token_ptr (1) = operand1_ptr;
	input_struc.operand.send_receive (1) = 0;	/*  sending  */
	input_struc.operand.size_sw (1) = 0;		/*  utility worries about size  */

	input_struc.operand.token_ptr (2) = operand2_ptr;
	input_struc.operand.send_receive (2) = 0;	/*  sending  */
	input_struc.operand.size_sw (2) = 0;

	input_struc.operand.token_ptr (3) = result_ptr;
	input_struc.operand.send_receive (3) = 1;	/*  receiving  */
	input_struc.operand.size_sw (3) = 0;

/*  Set the proper opcode into the eis instruction  */
	if opcode_code = 1				/*  add  */
	then inst_struc.fill1_op = add3_op;
	else inst_struc.fill1_op = subtract3_op;

/*  Establish the addresses  */

	call cobol_addr (input_ptr, inst_ptr, reloc_ptr);

/*  Set the rounding bit in the eis instruction if necessary  */
	if result_ptr -> data_name.rounded
	then inst_struc.zero3 = "01"b;		/*  TRUNCATION OFF, ROUNDING ON  */

/*  Emit the eis instruction and 3 descriptors  */

	call cobol_emit (inst_ptr, reloc_ptr, 4);

	/***..... if Trace_Bit then call cobol_gen_driver_$Tr_End(MY_NAME);/**/
	return;


	/***.....	dcl cobol_gen_driver_$Tr_Beg entry(char(*));/**/
	/***.....	dcl cobol_gen_driver_$Tr_End entry(char(*));/**/

	/***.....	dcl Trace_Bit bit(1) static external;/**/
	/***.....	dcl Trace_Lev fixed bin static external;/**/
	/***.....	dcl Trace_Line char(36) static external;/**/
	/***.....	dcl ioa_ entry options(variable); /**/
	/***..... dcl MY_NAME char (10) int static init ("COBOL_ADD3");/**/


/*  INCLUDE FILES USED BY THIS PROCEDURE  */


/*****	Declaration for builtin function	*****/

dcl	(substr, mod, binary, fixed, addr, addrel, rel, length, string, unspec, null, index)
			builtin;

/*****	End of declaration for builtin function	*****/

%include cobol_type9;


%include cobol_addr_tokens;

/**************************************************/
/*	 END OF PROCEDDURE			*/
/*	cobol_add3				*/
/*************************************************/

     end cobol_add3;
