/****^  ***********************************************************
        *                                                         *
        * Copyright, (C) BULL HN Information Systems Inc., 1989   *
        *                                                         *
        * Copyright, (C) Honeywell Information Systems Inc., 1982 *
        *                                                         *
        * Copyright (c) 1972 by Massachusetts Institute of        *
        * Technology and Honeywell Information Systems, Inc.      *
        *                                                         *
        *********************************************************** */




/****^  HISTORY COMMENTS:
  1) change(89-04-23,Zimmerman), approve(89-04-23,MCR8060),
     audit(89-05-05,RWaters), install(89-05-24,MR12.3-1048):
     MCR8060 cobol_def_init.pl1 Reformatted code to new Cobol standard.
                                                   END HISTORY COMMENTS */


/* Modified on 01/14/77 by ORN to signal command_abort rather than cobol_compiler_error */
/* Modified since Version 2.0 */

/*{*/
/* format: style3 */
cobol_def_init:
     proc;					/*
The procedure cobol_def_init initializes the Definition Section and
outputs corresponding relocation information.  In particular it:

     1.  Initializes all items in the header and inserts  a word
         of zeros following the header.

     2.  Creates a class-3 definition for the object segment.

     3.  Creates a class-2 definition for the symbol-table.

     4.  Creates an acc string for "symbol_table".

     5.  Creates an acc string for the object segment name.

These entities are positioned in the Definition Section in the
order given.  cobol_def_init is called once per compilation, prior
to code generation.

Note: The class-0 definition for the entry point must immediately
      follow the acc string for the object segment name; i.e. it 
      must be located at def_wd_off as it exists upon return from 
      cobol_def_init.


U___s_a_g_e:_

     declare cobol_def_init  entry;

     call cobol_def_init;


D__a_t_a:_


     % include cobol_;

	Items in cobol_ include file used (u) and/or set (s) by
	cobol_def_init:

	     cobol_ptr (u)
	     com_ptr (u)
	     def_base_ptr (u)
	     def_wd_off (s)
	     def_max (u)
	     obj_seg_name (u)
						  */

%include cobol_definitions;


dcl	1 error_s		aligned static,
	  2 my_name	char (32) init ("cobol_def_init"),
	  2 message_len	fixed bin init (35),
	  2 message	char (168) init ("Definition Section length exceeded!");

dcl	zeros		aligned bit (36) aligned based (def_ptr);
dcl	len_segname_acc_str fixed bin;
dcl	wd_len		fixed bin;
dcl	temp		fixed bin;

dcl	1 acc		aligned based (def_ptr),
	  2 length_of_string
			fixed bin (8) unaligned,
	  2 string	char (0 refer (acc.length_of_string)) unaligned;

dcl	reloc_info	(44) bit (5) aligned static init ("10101"b, "00000"b,
						/* for		*/
			"00000"b, "00000"b,		/* header		*/
			"00000"b, "00000"b,		/* for wd of zeros  */
			"10101"b, "10101"b,		/* for		*/
			"10101"b, "00000"b,		/* class-3	*/
			"10101"b, "10101"b,		/* definition	*/
			"10101"b, "10101"b,		/* for		*/
			"10110"b, "00000"b,		/* class-2	*/
			"10101"b, "10101"b,		/* definition	*/
			"00000"b, "00000"b,		/* for		*/
			"00000"b, "00000"b,		/* symbol_table	*/
			"00000"b, "00000"b,		/* acc		*/
			"00000"b, "00000"b,		/* string		*/
			"00000"b, "00000"b,		/* for		*/
			"00000"b, "00000"b,		/* object segment	*/
			"00000"b, "00000"b,		/* name		*/
			"00000"b, "00000"b,		/* acc string	*/
			"00000"b, "00000"b,		/* allows 	*/
			"00000"b, "00000"b,		/* for		*/
			"00000"b, "00000"b,		/* 32_character	*/
			"00000"b, "00000"b,		/* max name	*/
			"00000"b, "00000"b);	/* length		*/

/*
P__r_o_c_e_d_u_r_e_s_C__a_l_l_e_d:_
						  */

dcl	signal_		entry (char (*), ptr, ptr);
dcl	cobol_reloc	entry (ptr, fixed bin, fixed bin);


/*
B__u_i_l_t-__i_n_F__u_n_c_t_i_o_n_s_U__s_e_d:_

						  */
dcl	null		builtin;
dcl	fixed		builtin;
dcl	substr		builtin;
dcl	unspec		builtin;
dcl	search		builtin;
dcl	string		builtin;
dcl	addrel		builtin;
dcl	addr		builtin;			/*}*/

%include cobol_;
%include cobol_fixed_common;
%include cobol_ext_;


/*************************************/
start:						/*  COMPUTE LENGTH OF ACC STRING FOR OBJECT SEGMENT NAME  */
	len_segname_acc_str = search (obj_seg_name, " ");

	if len_segname_acc_str = 0
	then do;

		len_segname_acc_str = 33;
		wd_len = 9;

	     end;

	else do;

		temp = len_segname_acc_str + 3;
		wd_len = fixed (substr (unspec (temp), 1, 34));

	     end;


/*  UPDATE def_wd_off AND TEST AGAINST def_max  */

	def_wd_off = wd_len + 13;

	if def_wd_off > def_max
	then do;

		call signal_ ("command_abort_", null, addr (error_s));
		return;

	     end;


/*  INITIALIZE HEADER  */

	def_list_relp = "000000000000000011"b;
	def_header.unused = (36)"0"b;
	def_header.flags.new_format = "1"b;
	def_header.flags.ignore = "1"b;
	def_header.flags.unused = (16)"0"b;


/*  INSERT WORD OF ZEROS AFTER HEADER  */

	def_ptr = addrel (def_base_ptr, 2);
	zeros = (36)"0"b;


/*  CREATE CLASS-3 DEFINITION FOR THE OBJECT SEGMENT  */

	def_ptr = addrel (def_ptr, 1);
	segname.forward_thread = "000000000000000110"b;
	segname.backward_thread = "000000000000000010"b;
	segname.segname_thread = "000000000000000010"b;
	segname.flags = "100000000000000"b;
	segname.class = "011"b;
	segname.symbol_relp = "000000000000001101"b;
	segname.first_relp = "000000000000000110"b;


/*  CREATE CLASS-2 DEFINITION FOR THE SYMBOL_TABLE  */

	def_ptr = addrel (def_ptr, 3);
	definition.forward_thread = "000000000000000010"b;
	definition.backward_thread = "000000000000000011"b;
	definition.value = (18)"0"b;
	string (definition.flags) = "100000000000000"b;
	definition.class = "010"b;
	definition.symbol_relp = "000000000000001001"b;
	definition.segname_relp = "000000000000000011"b;


/*  CREATE ACC STRING FOR "symbol_table"  */

	def_ptr = addrel (def_ptr, 3);
	length_of_string = 12;
	acc.string = "symbol_table";


/*  CTEATE ACC STRING FOR OBJECT SEGMENT NAME  */

	def_ptr = addrel (def_ptr, 4);
	length_of_string = len_segname_acc_str - 1;
	acc.string = obj_seg_name;


/*  OUTPUT RELOCATION INFORMATIION  */

	call cobol_reloc (addr (reloc_info), def_wd_off + def_wd_off, 3003);


/*  INITIALIZATION COMPLETE  */
exit:
	return;

     end cobol_def_init;
