/****^  ***********************************************************
        *                                                         *
        * Copyright, (C) BULL HN Information Systems Inc., 1989   *
        *                                                         *
        * Copyright, (C) Honeywell Information Systems Inc., 1982 *
        *                                                         *
        * Copyright (c) 1972 by Massachusetts Institute of        *
        * Technology and Honeywell Information Systems, Inc.      *
        *                                                         *
        *********************************************************** */




/****^  HISTORY COMMENTS:
  1) change(89-04-23,Zimmerman), approve(89-04-23,MCR8060),
     audit(89-05-05,RWaters), install(89-05-24,MR12.3-1048):
     MCR8060 cobol_delete_gen.pl1 Reformatted code to new Cobol standard.
                                                   END HISTORY COMMENTS */


/* Modified on 03/18/81 by FCH, [4.4-1], set pr1 to loc of key */
/* Modified on 06/27/79 by FCH, [4.0-1], not option added for debug */
/* Modified on 11/13/78 by FCH, [3.0-1], alt rec keys added */
/* Modified since Version 3.0 */

/* format: style3 */
cobol_delete_gen:
     proc (mp_ptr, passed_tag);

dcl	passed_tag	fixed bin;		/* for  in-line error handling */
dcl	ptag		fixed bin;
dcl	mp_ptr		ptr;
dcl	1 mp		based (mp_ptr),
	  2 n		fixed bin,		/* always 3 */
	  2 pt1		ptr,			/* pts to type1 token for DELETE */
	  2 pt2		ptr,			/* pts to type12 token for the file */
	  2 pt3		ptr;			/* pts to type19 token (eos) */
dcl	1 args,
	  2 entryno	fixed bin,
	  2 arglist_off	fixed bin,
	  2 stacktemp_off	fixed bin,
	  2 n		fixed bin,
	  2 arg		(4),
	    3 pt		ptr,
	    3 type	fixed bin,
	    3 off1	fixed bin,
	    3 off2	fixed bin,
	    3 value	bit (18) unal,
	    3 indirect	bit (1) unal,
	    3 overlay	bit (1) unal,
	    3 repeat_nogen	bit (1) unal,
	    3 regsw	bit (1) unal,
	    3 regno	bit (3) unal;


dcl	argb		(4) bit (216) based (addr (args.arg (1)));

dcl	text		(0:100000) bit (36) based (cobol_$text_base_ptr);

dcl	ft_ptr		ptr;

dcl	fkey_ptr		ptr;

dcl	name_ptr		ptr;
dcl	arg_ptr		ptr;

dcl	ioerror_ptr	ptr;

/* [3.0-1] */
declare	alt_sw		bit (1);

dcl	delete_tag	fixed bin;

dcl	unopen_error_tag	fixed bin;
dcl	stoff		fixed bin;

dcl	aloff		fixed bin;
dcl	size		fixed bin;
dcl	reclen_off	fixed bin;

dcl	buf_off		fixed bin;
dcl	ntag		fixed bin;

/*************************************/
/*************************************/
/* INITIALIZATION */
start:
	rw_ptr = mp.pt1;
	eos_ptr = mp.pt3;
	ioerror_ptr = addr (ioerror);
	ioerror.cobol_code = 0;
	ioerror.type1_ptr = mp.pt1;
	ioerror.is_tag = 0;
	ioerror.mode = 0;
	if end_stmt.b = "1"b
	then do;					/* in-line error coding follows */
		ioerror.is_tag = cobol_$next_tag;	/* to be defined at end of generated code for WRITE */
		ptag, passed_tag = cobol_$next_tag + 1; /* to be defined by gen driver at end of in-line coding */
		ioerror.ns_tag = ptag;
		cobol_$next_tag = cobol_$next_tag + 2;
	     end;
	else do;
		ioerror.is_tag = 0;
		ptag = 0;
		ioerror.ns_tag = cobol_$next_tag;	/* to be defined at end of generated code */
		cobol_$next_tag = cobol_$next_tag + 1;
	     end;

	arg_ptr = addr (args);

	call cobol_read_ft (mp.pt2 -> fd_token.file_no, ft_ptr);

	call cobol_alloc$stack (52, 2, aloff);		/* enough for 13 words - aloff is a wd offset */
	args.arglist_off = aloff;
	reclen_off = aloff + 12;


/*************************************/
/* START CODE GENERATION */
start_codegen:					/* MAKE SURE FILE IS OPEN */
	ioerror.retry_tag = cobol_$next_tag;
	unopen_error_tag = cobol_$next_tag + 1;
	cobol_$next_tag = cobol_$next_tag + 2;

	call cobol_define_tag (ioerror.retry_tag);

	call cobol_set_fsbptr (ft_ptr);


/* OPERATOR56(init_delete) */
	call cobol_call_op (56, unopen_error_tag);	/* INT_DELETE_OP */

	call cobol_gen_ioerror (ft_ptr, ioerror_ptr);

	call cobol_define_tag (unopen_error_tag);

/* [3.0-1] */
	alt_sw = file_table.organization = 3 /* ind */ /* [3.0-1] */ & /* [3.0-1] */ file_table.alternate_keys ^= 0;




	if file_table.access < 2
	then do;					/* sequential access */

		ntag = cobol_$next_tag;

		cobol_$next_tag = cobol_$next_tag + 1;

		call cobol_io_util$bypass_seqerror (ntag);

		call cobol_ioop_util$set_x5 (delete_seq_errno);
						/* OPERATOR54(delete_error) */
		call cobol_call_op (54, ntag);	/* ERROR_OP */

		call cobol_gen_ioerror (ft_ptr, ioerror_ptr);



		call cobol_define_tag (ntag);

		call cobol_io_util$move_direct ("001"b, fsb_keylen_sw, 4, 1, ""b);
						/* zero the switch */

		call cobol_set_fsbptr (ft_ptr);

		call del_op;

	     end;

	else do;					/* random or dynamic access */

		call cobol_read_rand (1, file_table.r_key_info, fkey_ptr);

		addr (fkey_type9.file_key_info) -> file_key_desc = file_key.desc;

		mpout.pt1 = mp.pt1;

		mpout.pt2 = addr (fkey_type9);

		if file_table.organization = 2
		then do;				/* relative */

			mpout.pt3 = addr (num_type9);

			size, num_type9.size, num_type9.places_left = 16;

			num_type9.seg = 5001;	/* from PR1 */

			num_type9.off = file_table.fsb.off + fsb_key;

		     end;

		else do;				/* indexed */

			if file_table.access = 3 & (file_table.external | file_table.open_out)
			then do;

				ntag = cobol_$next_tag;
				cobol_$next_tag = cobol_$next_tag + 1;

				call cobol_io_util$bypass_mode_error (ntag, "11"b);
						/* must prevent deletes in output mode */

				call cobol_ioop_util$set_x5 (output_errno);
						/* OPERATOR54(delete_error) */
				call cobol_call_op (54, ntag);
						/* ERROR_OP */

				call cobol_gen_ioerror (ft_ptr, ioerror_ptr);

				call cobol_define_tag (ntag);

				call cobol_set_fsbptr (ft_ptr);
			     end;

			mpout.pt3 = addr (alpha_type9);
			size, alpha_type9.size = fkey_type9.size;
			alpha_type9.seg = 5001;	/* from pr1 */
			alpha_type9.off = file_table.fsb.off + fsb_key;

		     end;				/* read key */
		if ^alt_sw & file_table.access = 3 & file_table.read_next
		then do;

			call cobol_alloc$stack (260, 2, stoff);
						/* area known as TEMP read key area */

			call cobol_ioop_util$lda_du (stoff);


			ntag = cobol_$next_tag;
			cobol_$next_tag = cobol_$next_tag + 1;

			call cobol_call_op (55, ntag);/* OPERATOR55(read_key) */

			call cobol_gen_ioerror (ft_ptr, ioerror_ptr);

			call cobol_define_tag (ntag);

			mpout.pt4 = addr (type19);

			call cobol_set_fsbptr (ft_ptr);

			call cobol_move_gen (addr (mpout));

			call cobol_io_util$move_direct ("001"b, fsb_keylen_sw, 4, 1, substr (unspec (size), 19, 18))
			     ;

			ntag = cobol_$next_tag;
			cobol_$next_tag = cobol_$next_tag + 1;

			call cobol_ioop_util$lda_du (stoff);

			call cobol_ioop_util$set_icode;

			call cobol_call_op (57, ntag);/* OPERATOR57(special_delete) */

			call cobol_gen_ioerror (ft_ptr, ioerror_ptr);

			call cobol_define_tag (ntag);

		     end;
		else do;

			ntag = cobol_$next_tag;
			cobol_$next_tag = cobol_$next_tag + 1;
			mpout.pt4 = addr (type19);

			call cobol_set_fsbptr (ft_ptr);

			call cobol_move_gen (addr (mpout));

			call cobol_io_util$move_direct ("001"b, fsb_keylen_sw, 4, 1, substr (unspec (size), 19, 18))
			     ;

/* [3.0-1] */
			if alt_sw			/*[4.4-1]*/
			then do;
				call cobol_io_util$fsb_key_loc (6);
						/* epp1 pr1|6 */
						/* [3.0-1] */
				call cobol_io_util$file_desc (file_table.file_desc_1_offset);
						/* [3.0-1] */
				call cobol_call_op (85, 0);
						/* OPERATOR85(alt_special_delete) */
						/* [3.0-1] */
				call cobol_set_fsbptr (ft_ptr);
						/* [3.01] */
			     end;

			call cobol_ioop_util$set_icode;

			call cobol_call_op (41, ntag);/* OPERATOR41(seek_key) */

			call cobol_gen_ioerror (ft_ptr, ioerror_ptr);

			call cobol_define_tag (ntag);

			call del_op;

		     end;

	     end;					/* DELETE THE RECORD */

	call cobol_reg_manager$after_op (4095 + ioerror.cobol_code);

/*[4.0-1]*/
	if end_stmt.f = "01"b			/*[4.0-1]*/
	then passed_tag = ioerror.is_tag;		/*[4.0-1]*/
	else call cobol_gen_ioerror$finish_up (ft_ptr, ioerror_ptr);

	return;

del_op:
     proc;

/* [3.0-1] */
	if alt_sw					/* [3.0-1] */
	then do;
		ntag = cobol_$next_tag;		/* [3.0-1] */
		cobol_$next_tag = cobol_$next_tag + 1;

/* [3.0-1] */
		call cobol_call_op (87, ntag);	/* OPERATOR87(alt_rew_del,ntag) */
						/* [3.0-1] */
		call cobol_gen_ioerror (ft_ptr, ioerror_ptr);
						/* [3.0-1] */
		call cobol_define_tag (ntag);		/* [3.0-1] */
		call cobol_set_fsbptr (ft_ptr);	/* [3.0-1] */
	     end;

	delete_tag = cobol_$next_tag;
	cobol_$next_tag = cobol_$next_tag + 1;

	call cobol_set_fsbptr (ft_ptr);

	call cobol_call_op (53, delete_tag);		/* OPERATOR53(delete) */

	call cobol_gen_ioerror (ft_ptr, ioerror_ptr);

	call cobol_define_tag (delete_tag);

/* [3.0-1] */
	if alt_sw
	then call cobol_call_op (86, 0);		/* OPERTATOR86(alt_delete) */

     end;

%include cobol_delete_gen_info;
%include cobol_delete_gen_data;
     end cobol_delete_gen;
