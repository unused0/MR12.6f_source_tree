04/05/85 boot

Syntax as a command:  boot {command} {keywords} {-control_arg}


Function:  boots Multics.  This command is valid at the "boot" and
"bce_crash" command levels.


Arguments:
command
   can be one of the following ring 1 command abbreviations:

         mult         multics
         salv         salvage_dirs
         stan         standard
         star         startup

keywords
   can be one or more of the following:
   nodt
      recreates the disk table; renames and ignores the existing one.


   nolv
      recreates the logical volume registration directory (>lv);
      renames and ignores the existing one.
   rlvs
      performs a volume salvage of the RPV (root physical volume), a
      directory salvage of all directories used in initialization, and
      a volume salvage of all other member volumes of the RLV (root
      logical volume).
   rpvs
      performs a volume salvage of the RPV and a directory salvage of
      all directories used in initialization.


Control arguments:
-cold
   specifies that the root directory is to be recreated, thus
   destroying the old file system hierarchy.  This option should only
   be used when a cold boot of BCE was also performed.  You will be
   asked whether BCE should continue.
