#!/bin/bash

# Edit the path names  
#   Where to mount root disk
S=./root/

#   Where to put the source tree
T=./source/

#   tapeUtil binaries
U=../tapeUtils/

# Get MFS

# $ git clone git@gitlab.com:dps8m/mfs.git
# $ cd mfs
# $ make
# $ cd ..

# Get a copy of the 12.6f root disk

# $ ln -s ../QuickStart_MR12.6f/root.dsk .

# Mount the root disk

# $ mkdir $S
# $ mfs/mfs root.dsk $S

# Extract source code (run this script)

# $ ./buildSource

# Unmount root disk

# $ fusermount -u $S


D="documentation system_library_obsolete system_library_tools library_dir_dir system_library_standard  system_library_unbundled system_library_3rd_party system_library_tandd"
# *.alm, *.pl1
(cd $S && find $D -type f \( -name "*.alm" -o \
                             -name "*.pl1" -o \
                             -name "*.info" -o \
                             -name "*.cds" -o \
                             -name "*.ec" -o \
                             -name "*.header" -o \
                             -name "*.search" -o \
                             -name "*.bind" -o \
                             -name "*.bindmap" -o \
                             -name "*.bindmap" -o \
                             -name "*.mexp" -o \
                             -name "*.lisp" -o \
                             -name "*.bcpl" -o \
                             -name "*.bcpl" -o \
                             -name "*.pascal" -o \
                             -name "*.fortran" -o \
                             -name "*.checker*" -o \
                             -name "*.header" -o \
                             -name "*.search" -o \
                             -name "*.list*" -o \
                             -name "*.table" -o \
                             -name "*.message" -o \
                             -name "*.control" -o \
                             -name "*.teco" -o \
                             -name "*.ttf" -o \
                             -name "*.xdw" -o \
                             -name "*.pnotice" -o \
                             -name "*.rtmf" -o \
                             -name "*.cmf" -o \
                             -name "*.absin" -o \
                             -name "*.ssl" -o \
                             -name "*.ti" -o \
                             -name "*.pldt" -o \
                             -name "*.qedx" -o \
                             -name "*.src" -o \
                             -name "*.ct" -o \
                             -name "*.absin"-o  \
                             -name "*.compin" -o \
                             -name "*.compout" -o \
                             -name "*.dcl" -o \
                             -name "*.ge" -o \
                             -name "*.iodt" -o \
                             -name "*.ld" -o \
                             -name "*.listin" -o \
                             -name "*.bind_fnp" -o \
                             -name "tut_" \
 \)  -print0) | xargs -0 -n 1 -I{} $U/p72_to_ascii $S/'{}' $T/'{}'

echo "Specials extract"

(cd $S && for N in \
           documentation/facilities_data_dir/edoc_db \
           documentation/facilities_data_dir/online_doc.cmdb \
           documentation/MR12.3/error_msgs.compout/0 \
           documentation/MR12.3/error_msgs.compout/1 \
           documentation/MR12.3/error_messages.toc.compout \
           documentation/MR12.3/SIB \
           documentation/MR12.3/SRB \
           documentation/MR12.3/system_book_ \
           documentation/MR12.3/TRs_fixed_in_MR12.3 \
           documentation/MR12.5/error_msgs.compout/0 \
           documentation/MR12.5/error_msgs.compout/1 \
           documentation/MR12.5/error_messages.toc.compout \
           documentation/MR12.5/MR12.5_SRB\SIB \
           documentation/MR12.5/system_book_ \
           documentation/MR12.6/error_msgs.compout/0 \
           documentation/MR12.6/error_msgs.compout/1 \
           documentation/MR12.6/error_msgs.toc.compout \
           documentation/MR12.6/MR12.6f_SIB \
           documentation/MR12.6/MR12.6f_SRB \
           documentation/MR12.6/system_book_ \
           documentation/MR12.6/Tickets_fixed_in_MR12.6f \
           documentation/subsystem/emacs/sample_start_up.emacs.lisp \
           library_dir_dir/crossref/total.crossref/0 \
           library_dir_dir/crossref/total.crossref/1 \
           library_dir_dir/crossref/total.crossref/2 \
           library_dir_dir/crossref/total.crossref/3 \
           library_dir_dir/mcs/info/macros_asm \
           library_dir_dir/mcs/info/macros.map355 \
           library_dir_dir/system_library_1/info/hardcore_checker_map \
           library_dir_dir/system_library_tools/object/psp_info_ \
           ; do
echo $N
             $U/p72_to_ascii $N ../$T/$N
           done)

echo "Archives extract"
# *.s.archive
(cd $S && find $D -type f -name "*.s.archive" -print0) | xargs -0 -n 1 -I{} $U/p72archive_to_ascii $S/'{}' $T/'{}'
(cd $S && find $D -type f -name "*.incl.archive" -print0) | xargs -0 -n 1 -I{} $U/p72archive_to_ascii $S/'{}' $T/'{}'
(cd $S && find $D -type f -name "maps.archive" -print0) | xargs -0 -n 1 -I{} $U/p72archive_to_ascii $S/'{}' $T/'{}'

# *.archive::*.bind
(cd $S && find $D -type f -name "*.archive" -print0) | xargs -0 -n 1 -I{} $U/p72archive_bind_to_ascii $S/'{}' $T/'{}'

chmod -w -R $T
